/**
 * Created by Jorge David Olmedo on 23/05/2018.
 */
var app = angular.module('bancard');
app.controller('horarioFechaIndex',function ($scope,kConstant,$http,$window,$filter,$timeout,getCursos) {
   
    $scope.obtener_entity = function (id) {
        $window.location.href = kConstant.url+"HorariosFecha/edit/"+id;
   }
   $scope.agregar_entity = function () {
       $window.location.href = kConstant.url+"HorariosFecha/add/";
   }
   $scope.ver_entity = function (id) {
       $window.location.href = kConstant.url+"HorariosFecha/view/"+id;
   }
   $scope.listar_entity = function () {
       $window.location.href = kConstant.url+"HorariosFecha/index/";
   }
   $scope.borrar_entity = function (id) {
       swal({
           title: "Deseas Anular el registro # "+id+"?",
           text: "Atencion. al anular el registro ya no podras recuperarlo!",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           confirmButtonText: "Si, eliminar!",
           closeOnConfirm: false
       }, function () {
           $http.post(kConstant.url+"HorariosFecha/deleteEntity/"+id).
           then(function(response){
               console.log(response);
               if(response.data.message=="ok"){
                   swal("Eliminado!", "El registro se elimino correctamente.", "success");
                   $timeout(function () {
                       $scope.listar_entity();
                   }, 1000);
               }
           },function (response) {
               console.log(response);
           });
       });
   }
   

});

app.controller('horarioFechaAdd',function ($scope,kConstant,$http,$window,$filter,$timeout,getCursos) {

    $scope.hfecha = {
        id:0,
        horario_inicio:"",
        horario_fin:"",
        fecha_curso_id:0,
        cupos:""
    }
   
    $scope.guardar = function(){
        if($scope.verificar_campos()){
            var horario_inicio = $scope.formatDateDMYHMS($("#inicio").val());
            var horario_fin = $scope.formatDateDMYHMS($("#fin").val());
            $scope.hfecha.horario_inicio = horario_inicio+":00";
            $scope.hfecha.horario_fin = horario_fin+":00";
            $scope.hfecha.fecha_curso_id = $scope.selectedCurso.id;
            console.log($scope.hfecha);
            $http.post(kConstant.url+"HorariosFecha/addEntity",$scope.hfecha).
            then(function(response){
                console.log(response);
                if(response.data.mensaje="ok") {
                    $window.location.href = kConstant.url+"HorariosFecha/index";
                }else{
                    toastr.error('No se puedieron guardar los datos.','Notificación!');
                }
    
            },function (response) {
    
            });  
        };
    }

    $scope.verificar_campos = function () {
        if($scope.hfecha==null || $scope.hfecha==""){
            toastr.error('Debes completar correctamente los datos.','Notificación!');
            $( "#cupos" ).focus();
            return false;
        };

        if($scope.hfecha.cupos==null || $scope.hfecha.cupos==""){
            toastr.error('Debes completar correctamente los datos.','Notificación!');
            $( "#cupo" ).focus();
            return false;
        };
        
        return true;
    }
    $scope.cursos=[];
    $scope.getFechaCursos=function(){
        $http.get(kConstant.url+"/FechasCurso/getFechaCursos/").
            then(function(response){
               $scope.cursos = response.data.fcursos;
               console.log($scope.cursos);
               $scope.selectedCurso = $scope.cursos[0];
            },function (response) {
                console.log("error");
            });
           
    }
    $scope.getFechaCursos();
    $scope.hasChanged = function() {
       $scope.fcurso.curso_id= $scope.selectedCurso.id;
    }

    $scope.formatDateYMD = function(date) {
        var fecha = date;
        fecha = fecha.split("/");
        fecha = fecha[2]+'-'+fecha[1]+'-'+fecha[0];
        return fecha;

    }

    $scope.formatDateDMY = function(date) {
        var d = new Date(date || Date.now()),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [day,month,year].join('/');
    }

    $scope.formatDateDMYHMS = function(date) {
        /*var d = date.split(" ");
        date = d[0];
        var fecha = date;
        fecha = fecha.split("/");
        fecha = fecha[2]+'-'+fecha[1]+'-'+fecha[0];*/
        
        return date;
        //return fecha+" "+d[1];
    }

   

    var date = new Date();
    $('#inicio').val("00:00");
    $('#fin').val("00:00");
    $("#cupo").focus();
    
});

app.controller('horarioFechaEdit',function ($scope,kConstant,$http,$window,$filter,$timeout,getCursos) {

    $scope.formatDateDMY = function(date){
        var fecha = date;
        fecha = fecha.substring(0,10);
        var hora = date;
        hora = hora.substring(11,16);
        fecha = fecha.split('-');
        fecha = hora;
        return fecha;
    }
    $scope.formatDateYMD = function(date) {
        var fecha = date;
        fecha = fecha.split("/");
        fecha = fecha[2]+'-'+fecha[1]+'-'+fecha[0];
        return fecha;

    }

    $scope.cursos=[];
    $scope.getFechasCurso=function(){
        $http.get(kConstant.url+"/FechasCurso/getFechaCursos/").
            then(function(response){
               $scope.cursos = response.data.fcursos;
               console.log($scope.fcursos);
             //  $scope.selectedCurso = $scope.cursos[0];
            },function (response) {
                console.log("error");
            });
           
    }
    
    $scope.hasChanged = function() {
      // $scope.fcurso.curso_id= $scope.selectedCurso.id;
    }

    $scope.hfecha = [];
    $scope.cargar_datos = function (id) {
        $scope.getFechasCurso();
        $scope.hfecha = [];
        $http.get(kConstant.url+"/HorariosFecha/getEntity/"+id)
            .then(function(data){
                
                $scope.hfecha = data.data.hcursos[0];
                //console.log($scope.hfecha);
                $("#inicio").val($scope.formatDateDMY($scope.hfecha.horario_inicio));
                $("#fin").val($scope.formatDateDMY($scope.hfecha.horario_fin));
                $scope.fechas_curso = $scope.hfecha.fechas_curso;
                console.log($scope.hfecha.fechas_curso);
               // console.log($scope.selectedCurso);
            });
      }

      $scope.formatDateDMYHMS = function(date) {
        /*var d = date.split(" ");
        date = d[0];
        var fecha = date;
        fecha = fecha.split("/");
        fecha = fecha[2]+'-'+fecha[1]+'-'+fecha[0];

        return fecha+" "+d[1];*/
        return date;
    }

      $scope.modificar = function (id) {
        if($scope.verificar_campos()){
            var horario_inicio = $scope.formatDateDMYHMS($("#inicio").val());
            var horario_fin = $scope.formatDateDMYHMS($("#fin").val());
            $scope.hfecha.horario_inicio = horario_inicio+":00";
            $scope.hfecha.horario_fin = horario_fin+":00";
            $scope.hfecha.fecha_curso_id = $scope.fechas_curso.id;
            console.log($scope.hfecha);
            delete $scope.hfecha.fechas_curso;
            $http.post(kConstant.url+"HorariosFecha/editEntity/"+id,$scope.hfecha).
            then(function(response){
                $window.location.href = kConstant.url+"HorariosFecha/index";
            },function (response) {

            });
        }
    }

    $scope.verificar_campos = function () {
        if($scope.hfecha==null || $scope.hfecha==""){
            toastr.error('Debes completar correctamente los datos.','Notificación!');
            $( "#cupos" ).focus();
            return false;
        };

        if($scope.hfecha.cupos==null || $scope.hfecha.cupos==""){
            toastr.error('Debes completar correctamente los datos.','Notificación!');
            $( "#cupo" ).focus();
            return false;
        };
        
        return true;
    }

});