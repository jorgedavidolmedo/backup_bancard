/**
 * Created by Jorge David Olmedo on 23/05/2018.
 */
var app = angular.module('bancard');
app.controller('fechaCursoIndex',function ($scope,kConstant,$http,$window,$filter,$timeout,getCursos) {

    
    $scope.obtener_entity = function (id) {
        $window.location.href = kConstant.url+"FechasCurso/edit/"+id;
   }
   $scope.agregar_entity = function () {
       $window.location.href = kConstant.url+"FechasCurso/add/";
   }
   $scope.ver_entity = function (id) {
       $window.location.href = kConstant.url+"FechasCurso/view/"+id;
   }
   $scope.listar_entity = function () {
       $window.location.href = kConstant.url+"FechasCurso/index/";
   }

   $scope.formatDateYMD = function(date) {
    var fecha = date;
    fecha = fecha.split("/");
    fecha = fecha[2]+'-'+fecha[1]+'-'+fecha[0];
    return fecha;

}

   $scope.verificar_fecha_edit = function(id,fecha){
    var fecha = $scope.formatDateYMD(fecha);
    $http.get(kConstant.url+"FechasCurso/verificarEdit/"+fecha).
        then(function(data){
         if(data.data.estado==1){
            $scope.obtener_entity(id);
         }else{
            swal("Atencion!", data.data.mensaje, "warning");
         }
        },function (response) {

        });
}

   $scope.borrar_entity = function (id) {
       swal({
           title: "Deseas Anular el registro # "+id+"?",
           text: "Atencion. al anular el registro ya no podras recuperarlo!",
           type: "warning",
           showCancelButton: true,
           confirmButtonColor: "#DD6B55",
           confirmButtonText: "Si, eliminar!",
           closeOnConfirm: false
       }, function () {
           $http.post(kConstant.url+"FechasCurso/deleteEntity/"+id).
           then(function(response){
               console.log(response);
               if(response.data.message=="ok"){
                   swal("Eliminado!", "El registro se elimino correctamente.", "success");
                   $timeout(function () {
                       $scope.listar_entity();
                   }, 1000);
               }
           },function (response) {
               console.log(response);
           });
       });
   }
   

});

app.controller('fechaCursoAdd',function ($scope,kConstant,$http,$window,$filter,$timeout,getCursos) {

    $scope.fcurso = {
        id:0,
        fecha:"",
        curso_id:0
    }

    $scope.verificar_fechas = function(){
        var fecha = $scope.formatDateYMD($("#fecha").val());
        $http.get(kConstant.url+"FechasCurso/verificar/"+fecha).
            then(function(data){
               if(data.data.estado==1){
                 $scope.guardarFechaPorCurso();
               }else{
                swal("Atencion!", data.data.mensaje, "warning");
               }
            },function (response) {
    
            });
    }

    $scope.guardarFechaPorCurso = function(){
        if($scope.verificar_campos()){
            var fecha = $scope.formatDateYMD($("#fecha").val());
            $scope.fcurso.fecha = fecha;
            $scope.fcurso.curso_id = $scope.selectedCurso.id;
            console.log($scope.fcurso);
            $http.post(kConstant.url+"FechasCurso/addEntity",$scope.fcurso).
            then(function(response){
                if(response.data.mensaje="ok") {
                    $window.location.href = kConstant.url+"FechasCurso/index";
                }else{
                    toastr.error('No se puedieron guardar los datos.','Notificación!');
                }
    
            },function (response) {
    
            });      
        };
    }

    $scope.verificar_campos = function () {
        if($scope.fcurso==null || $scope.fcurso==""){
            toastr.error('Debes completar correctamente los datos.','Notificación!');
            $( "#nombre" ).focus();
            return false;
        };
        if($("#fecha").val()==null || $("#fecha").val()==""){
            toastr.error('El campo fecha no puede estar vacio.','Notificación!');
            $( "#fecha" ).focus();
            return false;
        };
        return true;
    }
    $scope.cursos=[];
    $scope.getCursos=function(){
        $http.get(kConstant.url+"/cursos/getCursos/").
            then(function(response){
               $scope.cursos = response.data.cursos;
               console.log($scope.cursos);
               $scope.selectedCurso = $scope.cursos[0];
            },function (response) {
                console.log("error");
            });
           
    }
    $scope.getCursos();
    $scope.hasChanged = function() {
       $scope.fcurso.curso_id= $scope.selectedCurso.id;
    }

    $scope.formatDateYMD = function(date) {
        var fecha = date;
        fecha = fecha.split("/");
        fecha = fecha[2]+'-'+fecha[1]+'-'+fecha[0];
        return fecha;

    }

    $scope.formatDateDMY = function(date) {
        var d = new Date(date || Date.now()),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

        if (month.length < 2) month = '0' + month;
        if (day.length < 2) day = '0' + day;

        return [day,month,year].join('/');
    }
    var date = new Date();
    $('#fecha').val($scope.formatDateDMY(date));
    $("#curso").focus();
    
});

app.controller('fechaCursoEdit',function ($scope,kConstant,$http,$window,$filter,$timeout,getCursos) {

    $scope.formatDateDMY = function(date){
        var fecha = date;
        fecha = fecha.substring(0,10);
        fecha = fecha.split('-');
        return (fecha[2]+"/"+fecha[1]+"/"+fecha[0]);
    }
    $scope.formatDateYMD = function(date) {
        var fecha = date;
        fecha = fecha.split("/");
        fecha = fecha[2]+'-'+fecha[1]+'-'+fecha[0];
        return fecha;

    }

    $scope.cursos=[];
    $scope.getCursos=function(){
        $http.get(kConstant.url+"/cursos/getCursos/").
            then(function(response){
               $scope.cursos = response.data.cursos;
               console.log($scope.cursos);
              // $scope.selectedCurso = $scope.cursos[0];
            },function (response) {
                console.log("error");
            });
           
    }
    
    $scope.hasChanged = function() {
      // $scope.fcurso.curso_id= $scope.selectedCurso.id;
    }

    $scope.fcurso = [];
    $scope.cargar_datos = function (id) {
        $scope.getCursos();
        $scope.fcurso = [];
        $http.get(kConstant.url+"/FechasCurso/getEntity/"+id)
            .then(function(data){
                console.log(data.data.fcurso[0]);
                $scope.fcurso = data.data.fcurso[0];
                $("#fecha").val($scope.formatDateDMY($scope.fcurso.fecha));
                $scope.fecha_inicial = $scope.formatDateDMY($scope.fcurso.fecha);
                $scope.id_curso = $scope.fcurso.curso_id;
                $timeout(function () {
                    $scope.selectedCurso = $scope.cursos[($scope.id_curso-1)];
                }, 500);
                
            });
      }

      $scope.verificar_fechas = function(id){
        var fecha = $scope.formatDateYMD($("#fecha").val());
        $http.get(kConstant.url+"FechasCurso/verificar/"+fecha).
            then(function(data){
               if(data.data.estado==1){
                $scope.modificar(id);
               }else{
                   if(data.data.estado==3){
                        if(data.data.fecha_actual==$scope.fecha_inicial){
                            $scope.modificar(id);
                        }
                   }else{   
                     swal("Atencion!", data.data.mensaje, "warning");
                   }
                
               }
            },function (response) {
    
            });
    }

      $scope.modificar = function (id) {
        if($scope.verificar_campos()){
            
            var fecha = $scope.formatDateYMD($("#fecha").val());
            $scope.fcurso.fecha = fecha;
            $scope.fcurso.curso_id = $scope.selectedCurso.id;
            delete $scope.fcurso.curso;
            $http.post(kConstant.url+"FechasCurso/editEntity/"+id,$scope.fcurso).
            then(function(response){
                $window.location.href = kConstant.url+"FechasCurso/index";
            },function (response) {

            });
        }
    }

    $scope.verificar_campos = function () {
        if($scope.fcurso==null || $scope.fcurso==""){
            toastr.error('Debes completar correctamente los datos.','Notificación!');
            $( "#nombre" ).focus();
            return false;
        };
        if($("#fecha").val()==null || $("#fecha").val()==""){
            toastr.error('El campo fecha no puede estar vacio.','Notificación!');
            $( "#fecha" ).focus();
            return false;
        };
        return true;
    }

});