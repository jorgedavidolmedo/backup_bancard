/**
 * Created by jorge on 12/28/16.
 */
var app = angular.module('bancard', []);
app.filter('startFrom', function () {
    return function (input, start) {
        if (input) {
            start = +start;
            return input.slice(start);
        }
        return [];
    };
}).constant("kConstant",{
    "url":"http://174.138.53.240/academia-bancard/",
    "base":"/"

}).service('getCursos',function($http,kConstant,$rootScope){
    return {
        async:function(term){
            return $http.get(kConstant.url+"/cursos/getCursos/");
        }
    }
});

