<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Log\Log;
use Cake\Datasource\ConnectionManager;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;
/**
 * HorariosFecha Controller
 *
 * @property \App\Model\Table\HorariosFechaTable $HorariosFecha
 */
class HorariosFechaController extends AppController
{

    public function beforeFilter(Event $event)
    {

        parent::beforeFilter($event);
        $this->Auth->allow(['getHorariosFecha','getHorariosFechaParam','getCupos','getFechas','getHorarios']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['FechasCurso','FechasCurso.Cursos'],
            'order'=>['HorariosFecha.id DESC'],
        ];
        $horariosFecha = $this->paginate($this->HorariosFecha);

        $this->set(compact('horariosFecha'));
        $this->set('_serialize', ['horariosFecha']);
    }

    /**
     * View method
     *
     * @param string|null $id Horarios Fecha id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $horariosFecha = $this->HorariosFecha->get($id, [
            'contain' => ['FechasCurso']
        ]);

        $this->set('horariosFecha', $horariosFecha);
        $this->set('_serialize', ['horariosFecha']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $horariosFecha = $this->HorariosFecha->newEntity();
        if ($this->request->is('post')) {
            $horariosFecha = $this->HorariosFecha->patchEntity($horariosFecha, $this->request->data);
            if ($this->HorariosFecha->save($horariosFecha)) {
                $this->Flash->success(__('The horarios fecha has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The horarios fecha could not be saved. Please, try again.'));
            }
        }
        $fechasCurso = $this->HorariosFecha->FechasCurso->find('list', ['limit' => 200]);
        $this->set(compact('horariosFecha', 'fechasCurso'));
        $this->set('_serialize', ['horariosFecha']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Horarios Fecha id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $horariosFecha = $this->HorariosFecha->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $horariosFecha = $this->HorariosFecha->patchEntity($horariosFecha, $this->request->data);
            if ($this->HorariosFecha->save($horariosFecha)) {
                $this->Flash->success(__('The horarios fecha has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The horarios fecha could not be saved. Please, try again.'));
            }
        }

        $fechasCurso = $this->HorariosFecha->FechasCurso->find('list', ['limit' => 200]);
        $this->set(compact('horariosFecha', 'fechasCurso'));
        $this->set('_serialize', ['horariosFecha']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Horarios Fecha id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $horariosFecha = $this->HorariosFecha->get($id);
        if ($this->HorariosFecha->delete($horariosFecha)) {
            $this->Flash->success(__('The horarios fecha has been deleted.'));
        } else {
            $this->Flash->error(__('The horarios fecha could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

     /********************************* SERVICES**********************************************/
    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function getFechaCursos()
    {
        if ($this->request->is('get')) {
            $conn = ConnectionManager::get('default');
            $conn->begin();

            try{
                $cursos = $this->Cursos->find("all");
                $mensaje = "202";
            }catch (\PDOException $e)
            {
                $conn->rollback();
                $mensaje = $e;
                $this->Flash->error(__('Error al guardar. vuelva a intentar.'.$e));
            }
        }
        $this->set([
            'mensaje' => $mensaje,
            'cursos' => $cursos,
            '_serialize' => ['mensaje', 'cursos']
        ]);
        $this->viewClass = 'Json';
        $this->render();

    }

     /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function addEntity()
    {
        if ($this->request->is('post')) {

            $conn = ConnectionManager::get('default');
            $conn->begin();

            try{

                $hcurso = $this->HorariosFecha->newEntity($this->request->data);

                if ($this->HorariosFecha->save($hcurso)) {
                    $conn->commit();
                    $mensaje = "ok";
                    $this->Flash->success(__('El registro se guardo correctamente.'));
                } else {
                    $conn->rollback();
                    $mensaje = "error";
                }

            }catch (\PDOException $e)
            {
                $conn->rollback();
                $mensaje = $e;
                $this->Flash->error(__('Error al guardar. vuelva a intentar.'.$e));
            }
        }

        $this->set([
            'mensaje' => $mensaje,
            'hcurso' => $hcurso,
            '_serialize' => ['mensaje', 'hcurso']
        ]);
        $this->viewClass = 'Json';
        $this->render();

    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function deleteEntity($id = null)
    {
        $message = "";
        $conn = ConnectionManager::get('default');
        try{
            $query = $conn->execute("DELETE FROM horarios_fecha WHERE id=".$id);
            if($query){
                $message = "ok";
            }else{
                $message = "no se pudo borrar el registro.";
            }

        }catch (\PDOException $e)
        {
            $mensaje = "error al eliminar.";
            $this->Flash->error(__('Error al eliminar. vuelva a intentar.'));
        }

        $this->set([
            'message' => $message,
            '_serialize' => ['message']
        ]);
        $this->viewClass = 'Json';
        $this->render();

    }

     /**
     * Add method
     * 
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function getEntity($id = null)
    {

        $hcursos = $this->HorariosFecha->find('all',
            array('conditions'=>array('HorariosFecha.id'=>$id),
                  'contain'=>array('FechasCurso')));

        $this->set([
            'hcursos' => $hcursos,
            '_serialize' => ['hcursos']
        ]);
        $this->viewClass = 'Json';
        $this->render();

    }


     /**
     * Edit method
     *
     * @param string|null $id Usuario id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function editEntity($id = null)
    {

        $fhora =$this->HorariosFecha->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            try{
                $fhora = $this->HorariosFecha->patchEntity($fhora, $this->request->data);
                if ($this->HorariosFecha->save($fhora)) {
                    $mensaje = "ok.";
                    $this->Flash->success(__('El registro se modifico correctamente.'));
                } else {
                    $mensaje = "error al modificar.";
                }

            }catch (\PDOException $e)
            {

                $mensaje = "error al editar.";
                $this->Flash->error(__('Error al editar. vuelva a intentar.'));
            }


        }

        $this->set([
            'mensaje' => $mensaje,
            'fhora' => $fhora,
            '_serialize' => ['mensaje', 'fhora']
        ]);
        $this->viewClass = 'Json';
        $this->render();
    }

     /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function getHorariosFecha()
    {
        $this->response->header(array(
            'Access-Control-Allow-Origin' => '*',
             'Access-Control-Allow-Headers' => 'Content-Type'
        ));
        if ($this->request->is('get')) {
            $conn = ConnectionManager::get('default');
            $conn->begin();
            $fechas_curso = [];
            try{
                 $sql = "SELECT a.id as id,
                 b.fecha as fecha,
                 a.horario_inicio as horario_inicio,
                 a.horario_fin as horario_fin,
                 c.nombre as curso
                 FROM horarios_fecha a, fechas_curso b,cursos c
                 WHERE
                 b.curso_id=c.id AND
                 a.fecha_curso_id=b.id";
                
                 $results = $conn->execute($sql);

                 $resultado = array();
                 foreach ($results as $value){
                     $resultado[] = array("id"=>$value['id'],
                        "curso"=>$value['curso'],
                        "fecha"=>date('d/m/Y',strtotime($value['fecha'])),
                        "horario_inicio"=>$value['horario_inicio'],
                        "horario_fin"=>$value['horario_fin']
                    );
                 }
                $fechas_curso = $resultado;
                $mensaje = "202";
            }catch (\PDOException $e)
            {
                $conn->rollback();
                $mensaje = $e;
                $this->Flash->error(__('Error al guardar. vuelva a intentar.'.$e));
            }
        }
        $this->set([
            'mensaje' => $mensaje,
            'fechas_curso' => $fechas_curso,
            '_serialize' => ['mensaje', 'fechas_curso']
        ]);
        $this->viewClass = 'Json';
        $this->render();

    }


     /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function getHorariosFechaParam($curso=null,$curso_id=null)
    {
        $this->response->header(array(
            'Access-Control-Allow-Origin' => '*',
             'Access-Control-Allow-Headers' => 'Content-Type'
        ));
        if ($this->request->is('get')) {
            $conn = ConnectionManager::get('default');
            $conn->begin();
            $fechas_curso = [];
            try{
                 $sql = "SELECT a.id as id,
                 b.fecha as fecha,
                 a.horario_inicio as horario_inicio,
                 a.horario_fin as horario_fin,
                 c.nombre as curso
                 FROM horarios_fecha a, fechas_curso b,cursos c
                 WHERE
                 b.curso_id=c.id AND
                 a.fecha_curso_id=".$curso_id." AND
                 a.fecha_curso_id=b.id";
                
                 $results = $conn->execute($sql);

                 $resultado = array();
                 foreach ($results as $value){
                     $resultado[] = array("id"=>$value['id'],
                        "curso"=>$value['curso'],
                        "fecha"=>date('d/m/Y',strtotime($value['fecha'])),
                        "horario_inicio"=>$value['horario_inicio'],
                        "horario_fin"=>$value['horario_fin']
                    );
                 }
                $fechas_curso = $resultado;
                $mensaje = "202";
            }catch (\PDOException $e)
            {
                $conn->rollback();
                $mensaje = $e;
                $this->Flash->error(__('Error al guardar. vuelva a intentar.'.$e));
            }
        }
        $this->set([
            'mensaje' => $mensaje,
            'fechas_curso' => $fechas_curso,
            '_serialize' => ['mensaje', 'fechas_curso']
        ]);
        $this->viewClass = 'Json';
        $this->render();

    }


     /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function getCupos($manana=null,$mediodia=null,$tarde=null)
    {
        $this->response->header(array(
            'Access-Control-Allow-Origin' => '*',
             'Access-Control-Allow-Headers' => 'Content-Type'
        ));
        $cupos = 0;
        if ($this->request->is('get')) {
            $conn = ConnectionManager::get('default');
            $conn->begin();
            $cupos = [];

            $desde = "";
            $hasta = "";

            if($manana=="true"){
                $desde = "08:00:00";
                $hasta = "10:59:00";
            }

            if($mediodia=="true"){
                $desde = "11:00:00";
                $hasta = "13:59:00";
            }

            if($tarde=="true"){
                $desde = "14:00:00";
                $hasta = "18:00:00";
            }

            if($manana=="true" && $mediodia=="true"){
                $desde = "08:00:00";
                $hasta = "13:59:00";
            }

            if($manana=="true" && $tarde=="true"){
                $desde = "08:00:00";
                $hasta = "18:00:00";
            }

            if($mediodia=="true" && $tarde=="true"){
                $desde = "11:00:00";
                $hasta = "18:00:00";
            }
         
            if($manana=="true" && $mediodia=="true" && $tarde=="true"){
               
                $desde = "08:00:00";
                $hasta = "18:00:00";
            }

            if($manana=="false" && $mediodia=="false" && $tarde=="false"){
               
                $desde = "08:00:00";
                $hasta = "18:00:00";
            }

          
            try{
                 $sql = "SELECT 
                 MAX(a.cupos) as cupos,
                 a.id as id,
                 SUM(b.cupos) as utilizados,
                 if(MAX(a.cupos)>SUM(b.cupos),(MAX(a.cupos) - SUM(b.cupos)),(SUM(b.cupos)-MAX(a.cupos))) as disponibles
                 FROM horarios_fecha a LEFT JOIN  inscripciones b ON b.horario_fecha_id = a.id
                 WHERE
                 a.horario_inicio >='".$desde."' AND 
                 a.horario_inicio<='".$hasta."' 
                 LIMIT 1";

                 $results = $conn->execute($sql);

                 $resultado = array();
                 foreach ($results as $value){
                     if($value['disponibles']==null){
                        $cupos = $value['cupos'];
                     }else{
                        $cupos = $value['disponibles'];
                     }
                     $resultado[] = array(
                        "id"=>$value['id'], 
                        "cupo"=>$cupos
                    );
                 }
                $cupos = $resultado;
                $mensaje = "202";
            }catch (\PDOException $e)
            {
                $conn->rollback();
                $mensaje = $e;
                $this->Flash->error(__('Error al guardar. vuelva a intentar.'.$e));
            }
        }
        $this->set([
            'mensaje' => $mensaje,
            'cupos' => $cupos,
            '_serialize' => ['mensaje', 'cupos']
        ]);
        $this->viewClass = 'Json';
        $this->render();

    }


     /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function getFechas($cupos=null,$manana=null,$mediodia=null,$tarde=null)
    {
        $this->response->header(array(
            'Access-Control-Allow-Origin' => '*',
             'Access-Control-Allow-Headers' => 'Content-Type'
        ));

        if($manana=="true"){
            $desde = "08:00:00";
            $hasta = "10:59:00";
        }

        if($mediodia=="true"){
            $desde = "11:00:00";
            $hasta = "13:59:00";
        }

        if($tarde=="true"){
            $desde = "14:00:00";
            $hasta = "18:00:00";
        }

        if($manana=="true" && $mediodia=="true"){
            $desde = "08:00:00";
            $hasta = "13:59:00";
        }

        if($manana=="true" && $tarde=="true"){
            $desde = "08:00:00";
            $hasta = "18:00:00";
        }

        if($mediodia=="true" && $tarde=="true"){
            $desde = "11:00:00";
            $hasta = "18:00:00";
        }
     
        if($manana=="true" && $mediodia=="true" && $tarde=="true"){
           
            $desde = "08:00:00";
            $hasta = "18:00:00";
        }

        if($manana=="false" && $mediodia=="false" && $tarde=="false"){
               
            $desde = "08:00:00";
            $hasta = "18:00:00";
        }

        if ($this->request->is('get')) {
            $conn = ConnectionManager::get('default');
            $conn->begin();
            $fechas = [];
     
            try{
                 $sql = "SELECT 
                 a.cupos as cupos,
                 b.fecha as fecha,
                 b.id as id
                 FROM horarios_fecha a, fechas_curso b
                 WHERE
                 a.fecha_curso_id=b.id AND
                 a.horario_inicio >='".$desde."' AND 
                 a.horario_inicio<='".$hasta."'AND
                 a.cupos>=".$cupos." GROUP BY a.fecha_curso_id";
                
                 $results = $conn->execute($sql);

                 $resultado = array();
                 foreach ($results as $value){
                     $resultado[] = array(
                        "id"=>$value['id'], 
                        "fecha"=>date('d/m/Y',strtotime($value['fecha'])),
                        "date"=>date('m/d/Y',strtotime($value['fecha']))
                    );
                     }

                $fechas = $resultado;
         
                $mensaje = "202";
            }catch (\PDOException $e)
            {
                $conn->rollback();
                $mensaje = $e;
                $this->Flash->error(__('Error al guardar. vuelva a intentar.'.$e));
            }
        }
        $this->set([
            'mensaje' => $mensaje,
            'fechas' => $fechas,
            '_serialize' => ['mensaje', 'fechas']
        ]);
        $this->viewClass = 'Json';
        $this->render();

    }

     /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function getHorarios($fecha_id=null,$manana=null,$mediodia=null,$tarde=null)
    {
        $this->response->header(array(
            'Access-Control-Allow-Origin' => '*',
             'Access-Control-Allow-Headers' => 'Content-Type'
        ));

        if($manana=="true"){
            $desde = "08:00:00";
            $hasta = "10:59:00";
        }

        if($mediodia=="true"){
            $desde = "11:00:00";
            $hasta = "13:59:00";
        }

        if($tarde=="true"){
            $desde = "14:00:00";
            $hasta = "18:00:00";
        }

        if($manana=="true" && $mediodia=="true"){
            $desde = "08:00:00";
            $hasta = "13:59:00";
        }

        if($manana=="true" && $tarde=="true"){
            $desde = "08:00:00";
            $hasta = "18:00:00";
        }

        if($mediodia=="true" && $tarde=="true"){
            $desde = "11:00:00";
            $hasta = "18:00:00";
        }
     
        if($manana=="true" && $mediodia=="true" && $tarde=="true"){
           
            $desde = "08:00:00";
            $hasta = "18:00:00";
        }

        if($manana=="false" && $mediodia=="false" && $tarde=="false"){
               
            $desde = "08:00:00";
            $hasta = "18:00:00";
        }

        if ($this->request->is('get')) {
            $conn = ConnectionManager::get('default');
            $conn->begin();
            $horarios = [];
     
            try{
                 $sql = "SELECT 
                 a.cupos as cupos,
                 b.fecha as fecha,
                 a.id as id,
                 a.horario_inicio as inicio,
                 a.horario_fin as fin
                 FROM horarios_fecha a, fechas_curso b
                 WHERE
                 a.fecha_curso_id=b.id AND
                 a.horario_inicio >='".$desde."' AND 
                 a.horario_inicio<='".$hasta."'AND
                 a.fecha_curso_id=".$fecha_id."";
                
                 $results = $conn->execute($sql);

                 $resultado = array();
                 foreach ($results as $value){
                     $resultado[] = array(
                        "id"=>$value['id'], 
                        "horarios"=>substr($value['inicio'],0,5)." - ".substr($value['fin'],0,5)
                    );
                 }
                $horarios = $resultado;
                $mensaje = "202";
            }catch (\PDOException $e)
            {
                $conn->rollback();
                $mensaje = $e;
                $this->Flash->error(__('Error al guardar. vuelva a intentar.'.$e));
            }
        }
        $this->set([
            'mensaje' => $mensaje,
            'horarios' => $horarios,
            '_serialize' => ['mensaje', 'horarios']
        ]);
        $this->viewClass = 'Json';
        $this->render();

    }


}
