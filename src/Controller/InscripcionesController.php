<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Log\Log;
use Cake\Datasource\ConnectionManager;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;

/**
 * Inscripciones Controller
 *
 * @property \App\Model\Table\InscripcionesTable $Inscripciones
 */
class InscripcionesController extends AppController
{

    public function beforeFilter(Event $event)
    {

        parent::beforeFilter($event);
        $this->Auth->allow(['addEntity','verificarInscripciones',
        'sendConfirmacion','obtener','obtenerFechaEnLetra',
        'conocerDiaSemanaFecha','sendRecordatorio',
        'verificarInscripcionesDia']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Cursos', 'FechasCurso', 'HorariosFecha']
        ];
        $inscripciones = $this->paginate($this->Inscripciones);

        $this->set(compact('inscripciones'));
        $this->set('_serialize', ['inscripciones']);
    }

    /**
     * View method
     *
     * @param string|null $id Inscripcione id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $inscripcione = $this->Inscripciones->get($id, [
            'contain' => ['Cursos', 'FechasCurso', 'HorariosFecha']
        ]);

        $this->set('inscripcione', $inscripcione);
        $this->set('_serialize', ['inscripcione']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $inscripcione = $this->Inscripciones->newEntity();
        if ($this->request->is('post')) {
            $inscripcione = $this->Inscripciones->patchEntity($inscripcione, $this->request->data);
            if ($this->Inscripciones->save($inscripcione)) {
                $this->Flash->success(__('The inscripcione has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The inscripcione could not be saved. Please, try again.'));
            }
        }
        $cursos = $this->Inscripciones->Cursos->find('list', ['limit' => 200]);
        $fechasCurso = $this->Inscripciones->FechasCurso->find('list', ['limit' => 200]);
        $horariosFecha = $this->Inscripciones->HorariosFecha->find('list', ['limit' => 200]);
        $this->set(compact('inscripcione', 'cursos', 'fechasCurso', 'horariosFecha'));
        $this->set('_serialize', ['inscripcione']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Inscripcione id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $inscripcione = $this->Inscripciones->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $inscripcione = $this->Inscripciones->patchEntity($inscripcione, $this->request->data);
            if ($this->Inscripciones->save($inscripcione)) {
                $this->Flash->success(__('The inscripcione has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The inscripcione could not be saved. Please, try again.'));
            }
        }
        $cursos = $this->Inscripciones->Cursos->find('list', ['limit' => 200]);
        $fechasCurso = $this->Inscripciones->FechasCurso->find('list', ['limit' => 200]);
        $horariosFecha = $this->Inscripciones->HorariosFecha->find('list', ['limit' => 200]);
        $this->set(compact('inscripcione', 'cursos', 'fechasCurso', 'horariosFecha'));
        $this->set('_serialize', ['inscripcione']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Inscripcione id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $inscripcione = $this->Inscripciones->get($id);
        if ($this->Inscripciones->delete($inscripcione)) {
            $this->Flash->success(__('The inscripcione has been deleted.'));
        } else {
            $this->Flash->error(__('The inscripcione could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

        /**
     * Index method
    *
    * @return \Cake\Network\Response|null
    */
    public function reporte()
    {
        $this->paginate = [
            'contain' => ['Cursos', 'FechasCurso', 'HorariosFecha']
        ];
        $inscripciones = $this->paginate($this->Inscripciones);

        $this->loadModel("FechasCurso");
       // $cursos = $this->FechasCurso->find('list',['keyField' => 'id','valueField' => 'fecha']);
           $conn = ConnectionManager::get('default');
            $conn->begin();
            $fcursos = [];
            /*$sql = "SELECT a.id as id,
                 a.fecha as fecha,
                 b.nombre as nombre,
                 a.curso_id as id_curso
                 FROM 
                 fechas_curso a,
                 cursos b,
                 inscripciones c
                 WHERE
                 a.id = c.horario_fecha_id AND 
                 a.curso_id = b.id";*/
                 $sql = "SELECT c.id as id,
                 c.fecha as fecha,
                 d.nombre as nombre,
                 d.id as id_curso
                 FROM 
                 inscripciones a,
                 horarios_fecha b,
                 fechas_curso c,
                 cursos d
                 WHERE
                 a.horario_fecha_id=b.id AND
                 a.fecha_curso_id=c.id AND 
                 a.curso_id=d.id
                 GROUP BY a.horario_fecha_id";
              
                 $results = $conn->execute($sql);

                 $resultado = array();
                 foreach ($results as $value){
                     $resultado[] = array("id"=>$value['id'],
                        "curso"=> $value['nombre'].' - ['. date('d/m/Y',strtotime($value['fecha'])).']',
                        "fecha"=>$value['fecha'],
                        "curso_id"=>$value['id_curso']);
                 }
                $cursos = $resultado;
        $this->set(compact('inscripciones','cursos','cursos'));
        $this->set('_serialize', ['inscripciones']);
    }

     /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function printReporte()
    {

        require_once(ROOT . DS . 'webroot' . DS . "class/tcpdf" . DS . "tcpdf.php");
        require_once(ROOT . DS . 'webroot' . DS . "class" . DS . "PHPJasperXML.inc.php");
        require_once(ROOT . DS . 'webroot' . DS . "class" . DS . "setting.php");

        try{


           $curso = $this->request->data['curso'];
           $reporte = $this->request->data['reporte'];

           $PHPJasperXML = new \PHPJasperXML();
           $results=null;
           $connection = ConnectionManager::get('default');


           if($reporte=="detallado"){
            $file = "informes/reporte.jrxml";    
            if($curso==0){
                $query="SELECT a.id as id,
                c.id as fecha_curso_id,
                a.nombre_apellido as nombre,
                CONCAT(d.nombre,' - ', DATE_FORMAT(c.fecha, '%d/%m/%Y') ) as fecha_curso_descripcion,
                c.fecha as fecha_curso,
                a.fecha as fecha,
                b.id as id_hora_curso,
                CONCAT(TIME_FORMAT(b.horario_inicio,'%H:%i'),' A ',TIME_FORMAT(b.horario_fin,'%H:%i')) as hora,
                a.celular as celular,
                a.correo as correo,
                CONCAT('Total de Cupos: ',b.cupos) as cupos_descrip,
                b.cupos as cupos,
                a.cupos as cupos_ins
                FROM inscripciones a, horarios_fecha b,fechas_curso c,cursos d
                WHERE 
                c.curso_id=d.id AND
                b.fecha_curso_id=c.id AND
                a.horario_fecha_id = b.id
                ORDER BY b.id,a.nombre_apellido";
               }else{
                $query="SELECT a.id as id,
                c.id as fecha_curso_id,
                a.nombre_apellido as nombre,
                CONCAT(d.nombre,' - ', DATE_FORMAT(c.fecha, '%d/%m/%Y') ) as fecha_curso_descripcion,
                c.fecha as fecha_curso,
                a.fecha as fecha,
                b.id as id_hora_curso,
                CONCAT(TIME_FORMAT(b.horario_inicio,'%H:%i'),' A ',TIME_FORMAT(b.horario_fin,'%H:%i')) as hora,
                a.celular as celular,
                a.correo as correo,
                b.cupos as cupos_descrip,
                b.cupos as cupos,
                a.cupos as cupos_ins
                FROM inscripciones a, horarios_fecha b,fechas_curso c,cursos d
                WHERE 
                c.curso_id=d.id AND
                b.fecha_curso_id=c.id AND
                a.horario_fecha_id = b.id AND
                a.fecha_curso_id=".$curso."
                ORDER BY b.id,a.nombre_apellido";
               }
           }else{
            $file = "informes/reporte_detallado.jrxml";
            if($curso==0){
                $query="SELECT a.id as id,
                c.id as fecha_curso_id,
                a.nombre_apellido as nombre,
                CONCAT(d.nombre,' ','-') as fecha_curso_descripcion,
                c.fecha as fecha_curso,
                a.fecha as fecha,
                b.id as id_hora_curso,
                CONCAT(TIME_FORMAT(b.horario_inicio,'%H:%i:%s'),' A ',TIME_FORMAT(b.horario_fin,'%H:%i:%s')) as hora,
                a.celular as celular,
                a.correo as correo,
                CONCAT('Cupos: ',b.cupos) as cupos_descrip,
                b.cupos as cupos,
                SUM(a.cupos) as contador,
                (b.cupos-SUM(a.cupos)) as saldo
                FROM inscripciones a, horarios_fecha b,fechas_curso c,cursos d
                WHERE 
                c.curso_id=d.id AND
                b.fecha_curso_id=c.id AND
                a.horario_fecha_id = b.id
                GROUP BY b.id
                ORDER BY b.id,a.nombre_apellido";
              
               }else{
                $query="SELECT a.id as id,
                c.id as fecha_curso_id,
                a.nombre_apellido as nombre,
                CONCAT(d.nombre,' ','-') as fecha_curso_descripcion,
                c.fecha as fecha_curso,
                a.fecha as fecha,
                b.id as id_hora_curso,
                CONCAT(TIME_FORMAT(b.horario_inicio,'%H:%i:%s'),' A ',TIME_FORMAT(b.horario_fin,'%H:%i:%s')) as hora,
                a.celular as celular,
                a.correo as correo,
                CONCAT('Cupos: ',b.cupos) as cupos_descrip,
                b.cupos as cupos,
                SUM(a.cupos) as contador,
                (b.cupos-SUM(a.cupos)) as saldo
                FROM inscripciones a, horarios_fecha b,fechas_curso c,cursos d
                WHERE 
                c.curso_id=d.id AND
                b.fecha_curso_id=c.id AND
                a.fecha_curso_id=".$curso." AND
                a.horario_fecha_id = b.id
                GROUP BY b.id
                ORDER BY b.id,a.nombre_apellido";
               }
           }
           
           /*$table = "CREATE TABLE table_name (
            column1 varchar(40),
            column2 INT,
            column3 varchar(20)
        )";*/
        //$connection->execute($table);
       

           
            /*************DATOS ADICIONALES*********************/
        
            $empresa_descripcion = "BANCARD S.A";
            $empresa_direccion = "Brasilia 765 e/ Republica de Siria y Fray Luis de Leon";
            $empresa_ruc = "80013884-8";
            $empresa_contacto = "(595 - 21) 416 - 1010";
           
            $hoy = date("Y-m-d");
            $hoy = explode("-",$hoy);
            $hoy = $hoy[2].'/'.$hoy[1].'/'.$hoy[0];

            $desde = explode("-",$desde);
            $desde = $desde[2].'/'.$desde[1].'/'.$desde[0];
            $hora = date("H:i:s");


            /***************************************************/
             $PHPJasperXML->arrayParameter=array("parameter1"=>1,"parametro_empresa"=>$empresa_descripcion,
            "factura"=>"Monto Credito: ",
            "nroFact"=>"Factura: ",
            "direccion"=>$empresa_direccion,
            "ruc"=>$empresa_ruc,
            "contacto"=>$empresa_contacto,
            "fecha"=>$hoy,
            "usuario"=>$this->Auth->user('email'),
            "query"=>$query,
            "cupos"=>"Cupos Habilitados",
            "utilizados"=>"Cantidad Inscriptos",
            "disponibles"=>"Cupos Disponibles",
            "empresa_software"=>"Todos los derechos reservados.",
            "empresa_descripcion"=>"Bancard S.A - (595-21)416-1010",
            "pagina"=>"Pag. Nro.",
            "total_factura"=>"Total Inscriptos: ",
            "fecha_impresion"=>"Fecha Impresion:".$hoy,
            "hora_impresion"=>"Hora Impresion:".$hora);
           
            $PHPJasperXML->load_xml_file($file);
            $server=$server;
            $db=$db;
            $user=$user;
            $pass=$pass;
            $version="0.9d";
            $pgport=5432;
            $pchartfolder="./class/pchart2";
            $PHPJasperXML->transferDBtoArray($server,$user,$pass,$db);

            //ob_end_clean();
            if (ob_get_length() > 0 ) {
                ob_end_clean();
            }
           
            $PHPJasperXML->outpage("I");


             }catch (\PDOException $e) {
             $error = 'No se puede borrar los datos. Esta asociado con otra clase.';
                // The exact error message is $e->getMessage();
                //var_dump($e);
               }



    }


    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function addEntity()
    {

        $this->response->header(array(
            'Access-Control-Allow-Origin' => '*',
             'Access-Control-Allow-Headers' => 'Content-Type'
        ));

        if ($this->request->is('post')) {

            $conn = ConnectionManager::get('default');
            $conn->begin();

            try{
                $horario_id = $this->request->data['horario_fecha_id'];
                $fecha_id = $this->request->data['fecha_curso_id'];
                $cupos = $this->request->data['cupos'];
                $correo = $this->request->data['correo'];

                $sql_disponible = "SELECT 
                (a.cupos) as disponibles
                FROM horarios_fecha a
                WHERE
                a.id=".$horario_id;
              
                $resultado_disponible = $conn->execute($sql_disponible);

                $disponibles = 0;
                foreach($resultado_disponible as $valor_disponible){
                    $disponibles = $valor_disponible['disponibles'];
                }
                
                $sql = "SELECT 
                SUM(a.cupos) as utilizados
                FROM inscripciones a
                WHERE
                a.fecha_curso_id=".$fecha_id." AND 
                a.horario_fecha_id=".$horario_id." 
                GROUP BY a.horario_fecha_id";
                $resultado = $conn->execute($sql);

                $utilizados = 0;
                foreach($resultado as $valor){
                    $utilizados = $valor['utilizados'];
                }
               
                if($disponibles>$utilizados){
                    $neto = $disponibles-$utilizados;
                }

                if($disponibles<$utilizados){
                    $neto = $utilizados-$disponibles;
                }
                
                $cupo_terminado=0;
                if($cupos>$neto){ 
                   $cupo_terminado=1;
                }

               
                if($cupo_terminado==0){
                        $inscripcion = $this->Inscripciones->newEntity($this->request->data);

                        if ($this->Inscripciones->save($inscripcion)) {
                            $conn->commit();
                            $mensaje = "202";

                            $sql_consulta = "SELECT c.id as id,
                            MAX(a.id) as referencia,
                            c.fecha as fecha,
                            d.nombre as nombre,
                            a.nombre_apellido as nombre_persona,
                            d.id as id_curso,
                            TIME_FORMAT(b.horario_inicio,'%H:%i') as hora_ini,
                            TIME_FORMAT(b.horario_fin,'%H:%i') as hora_fin,
                            a.correo as correo
                            FROM 
                            inscripciones a,
                            horarios_fecha b,
                            fechas_curso c,
                            cursos d
                            WHERE
                            a.horario_fecha_id=b.id AND
                            a.fecha_curso_id=c.id AND 
                            a.curso_id=d.id AND 
                            a.fecha_curso_id=".$fecha_id." AND 
                            a.horario_fecha_id=".$horario_id." 
                            GROUP BY c.id ORDER BY a.id DESC LIMIT 1";
                           
                            $results_consulta = $conn->execute($sql_consulta);
                            $fecha_curso = "";
                            $hora_curso= "";
                            $nombre_persona = "";
                            $nombre_curso = "";
                           
                            foreach ($results_consulta as $value_c){
                               $fecha_curso = $value_c['fecha'];
                               $hora_curso_ini = $value_c['hora_ini'];
                               $hora_curso_fin = $value_c['hora_fin'];
                               $nombre_persona = $value_c['nombre_persona'];
                               $nombre_curso = $value_c['nombre'];
                            }
                           
                            $values = array (
                                "nombre"=>$nombre_curso,
                                "nombre_persona"=>$nombre_persona,
                                "fecha"=>date('d/m/Y',strtotime($fecha_curso)),
                                "hora_ini"=>$hora_curso_ini,
                                "hora_fin"=>$hora_curso_fin,
                                "to" => $correo
                            );
                    
                        $this->sendConfirmacion($values);
                        } else {
                            $conn->rollback();
                            $mensaje = "error";
                        }
                }else{
                    $mensaje = "No se puede guardar los datos. Ya no existe cupo disponible en este horario.";
                    $inscripcion = [];
                }
                

            }catch (\PDOException $e)
            {
                $conn->rollback();
                $mensaje = $e;
            }
        }

        $this->set([
            'mensaje' => $mensaje,
            'inscripcion' => $inscripcion,
            '_serialize' => ['mensaje', 'inscripcion']
        ]);
        $this->viewClass = 'Json';
        $this->render();

    }

    function conocerDiaSemanaFecha($fecha) {
       
        $dias = array('Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado');
    
        $dia = $dias[date('w', strtotime($fecha))];
    
        return $dia;
    
    }

    function obtenerFechaEnLetra($fecha){
       
        $dia= $this->conocerDiaSemanaFecha($fecha);
    
        $num = date("j", strtotime($fecha));
    
        $anno = date("Y", strtotime($fecha));
    
        $mes = array('enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre');
    
        $mes = $mes[(date('m', strtotime($fecha))*1)-1];
    
        return $dia.', '.$num.' de '.$mes.' del '.$anno;
    
    }
    
     

    



     /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function verificarInscripciones()
    {

        $this->response->header(array(
            'Access-Control-Allow-Origin' => '*',
             'Access-Control-Allow-Headers' => 'Content-Type'
        ));

        if ($this->request->is('get')) {
            $conn = ConnectionManager::get('default');
            $conn->begin();

            try{

                            $hoy = date("Y-m-d");
                           
                            $sql_consulta = "SELECT c.id as id,
                            a.id as referencia,
                            c.fecha as fecha,
                            d.nombre as nombre,
                            a.nombre_apellido as nombre_persona,
                            d.id as id_curso,
                            CONCAT('Desde las ',b.horario_inicio,' Hasta las ',b.horario_fin) as hora,
                            TIME_FORMAT(b.horario_inicio,'%H:%i') as hora_ini,
                            TIME_FORMAT(b.horario_fin,'%H:%i') as hora_fin,
                            a.correo as correo
                            FROM 
                            inscripciones a,
                            horarios_fecha b,
                            fechas_curso c,
                            cursos d
                            WHERE
                            a.horario_fecha_id=b.id AND
                            a.fecha_curso_id=c.id AND 
                            a.curso_id=d.id
                            GROUP BY a.correo,a.fecha_curso_id, a.horario_fecha_id ORDER BY a.id";
                           
                            $results_consulta = $conn->execute($sql_consulta);
                            $fecha_curso = "";
                            $hora_curso= "";
                            $nombre_persona = "";
                            $nombre_curso = "";

                            foreach ($results_consulta as $value_c){
                               $fecha_curso = $value_c['fecha'];
                               $hora_curso = $value_c['hora'];
                               $nombre_persona = $value_c['nombre_persona'];
                               $nombre_curso = $value_c['nombre'];
                               $hora_ini = $value_c['hora_ini'];
                               $hora_fin = $value_c['hora_fin'];

                               $dias=(strtotime($hoy)-strtotime($value_c['fecha']))/86400;
                               $dias=abs($dias); 
                               $dias=floor($dias);
                               //echo $hoy." - ".$value_c['fecha']."<br>";
                               //echo $dias;
                              // die();
                               if($dias==1){
                                    $mensaje = '
                                    <b>'.$nombre_persona.'</b>, Te recordamos que mañana '.date('d/m/Y',strtotime($fecha_curso)).' es tu capacitación en la Academia Bancard. El horario elegido es de '.$hora_ini.' hs. a '.$hora_fin.' hs.
                                    ';
                                    $values = array (
                                        "nombre"=>$nombre_curso,
                                        "nombre_persona"=>$nombre_persona,
                                        "fecha"=>date('d/m/Y',strtotime($fecha_curso)),
                                        "hora"=>$hora_curso,
                                        "to" => $value_c['correo'],
                                        "mensaje"=>$mensaje,
                                        "subject"=>"Mañana es tu capacitación en la Academia Bancard"
                                    );
                                    $this->sendRecordatorio($values);
                               } 

                               
                 }
                   
            }catch (\PDOException $e)
            {
                
                $conn->rollback();
                $mensaje = $e;
            }
        }

        $this->set([
            'mensaje' => $mensaje,
            '_serialize' => ['mensaje']
        ]);
        $this->viewClass = 'Json';
        $this->render();

    }



     /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function verificarInscripcionesDia()
    {

        $this->response->header(array(
            'Access-Control-Allow-Origin' => '*',
             'Access-Control-Allow-Headers' => 'Content-Type'
        ));

        if ($this->request->is('get')) {
            $conn = ConnectionManager::get('default');
            $conn->begin();

            try{

                            $hoy = date("Y-m-d");
                           
                            $sql_consulta = "SELECT c.id as id,
                            a.id as referencia,
                            c.fecha as fecha,
                            d.nombre as nombre,
                            a.nombre_apellido as nombre_persona,
                            d.id as id_curso,
                            CONCAT('Desde las ',b.horario_inicio,' Hasta las ',b.horario_fin) as hora,
                            TIME_FORMAT(b.horario_inicio,'%H:%i') as hora_ini,
                            TIME_FORMAT(b.horario_fin,'%H:%i') as hora_fin,
                            a.correo as correo
                            FROM 
                            inscripciones a,
                            horarios_fecha b,
                            fechas_curso c,
                            cursos d
                            WHERE
                            a.horario_fecha_id=b.id AND
                            a.fecha_curso_id=c.id AND 
                            a.curso_id=d.id
                            GROUP BY a.correo,a.fecha_curso_id, a.horario_fecha_id ORDER BY a.id";
                           
                            $results_consulta = $conn->execute($sql_consulta);
                            $fecha_curso = "";
                            $hora_curso= "";
                            $nombre_persona = "";
                            $nombre_curso = "";

                            foreach ($results_consulta as $value_c){
                               $fecha_curso = $value_c['fecha'];
                               $hora_curso = $value_c['hora'];
                               $nombre_persona = $value_c['nombre_persona'];
                               $nombre_curso = $value_c['nombre'];
                               $hora_ini = $value_c['hora_ini'];
                               $hora_fin = $value_c['hora_fin'];

                               $dias=(strtotime($hoy)-strtotime($value_c['fecha']))/86400;
                               $dias=abs($dias); 
                               $dias=floor($dias);
                              
                            if($dias==0){

                                $hora = date("H:i");
                                $hora_hoy = $hora;
                                $ts_fin = strtotime($hoy." ".$hora_ini);
                                $ts_ini = strtotime($hoy." ".$hora_hoy);
                                $dif_hora = ($ts_fin-$ts_ini)/3600;  
                                $dif_hora = round($dif_hora);  
                               
                                if($dif_hora==2){
                                    $mensaje = '
                                <b>'.$nombre_persona.'</b>, te esperamos en el edificio Bancard (Avda. Brasilia 765 c/Siria) hoy '.date('d/m/Y',strtotime($fecha_curso)).' de '.$hora_ini.' hs. a '.$hora_fin.' hs.
                                ';
                                $values = array (
                                    "nombre"=>$nombre_curso,
                                    "nombre_persona"=>$nombre_persona,
                                    "fecha"=>date('d/m/Y',strtotime($fecha_curso)),
                                    "hora"=>$hora_curso,
                                    "to" => $value_c['correo'],
                                    "mensaje"=>$mensaje,
                                    "subject"=>"En 2 horas empieza tu capacitación en la Academia
                                    Bancard"
                                    
                                );
                                  $this->sendRecordatorio($values);
                                }
                                
                           } 
                              //  die();
                               
                 }
                   
            }catch (\PDOException $e)
            {
                
                $conn->rollback();
                $mensaje = $e;
            }
        }

        $this->set([
            'mensaje' => $mensaje,
            '_serialize' => ['mensaje']
        ]);
        $this->viewClass = 'Json';
        $this->render();

    }


     /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */

    public function excel($curso = null)
    {
        require_once(ROOT . DS . 'webroot' . DS . "class/tcpdf" . DS . "tcpdf.php");
        require_once(ROOT . DS . 'webroot' . DS . "class" . DS . "PHPJasperXML.inc.php");
        require_once(ROOT . DS . 'webroot' . DS . "class" . DS . "setting.php");
 
        try{
        
                $PHPJasperXML = new \PHPJasperXML();
 

                if($curso==0){
                    $query = "SELECT a.id as id,
                c.id as fecha_curso_id,
                a.nombre_apellido as nombre,
                CONCAT(d.nombre,' - ', DATE_FORMAT(c.fecha, '%d/%m/%Y') ) as fecha_curso_descripcion,
                c.fecha as fecha_curso,
                a.fecha as fecha,
                b.id as id_hora_curso,
                CONCAT(TIME_FORMAT(b.horario_inicio,'%H:%i'),' - ',TIME_FORMAT(b.horario_fin,'%H:%i')) as hora,
                a.celular as celular,
                a.correo as email,
                CONCAT('Total de Cupos: ',b.cupos) as cupos_descrip,
                b.cupos as cupos,
                a.cupos as cupos_ins,
                c.fecha as fecha_hora
                FROM inscripciones a, horarios_fecha b,fechas_curso c,cursos d
                WHERE 
                c.curso_id=d.id AND
                b.fecha_curso_id=c.id AND
                a.horario_fecha_id = b.id
                ORDER BY b.id,a.nombre_apellido";

                }else{

                $query = "SELECT a.id as id,
                c.id as fecha_curso_id,
                a.nombre_apellido as nombre,
                CONCAT(d.nombre,' - ', DATE_FORMAT(c.fecha, '%d/%m/%Y') ) as fecha_curso_descripcion,
                c.fecha as fecha_curso,
                a.fecha as fecha,
                b.id as id_hora_curso,
                CONCAT(TIME_FORMAT(b.horario_inicio,'%H:%i'),' - ',TIME_FORMAT(b.horario_fin,'%H:%i')) as hora,
                a.celular as celular,
                a.correo as email,
                CONCAT('Total de Cupos: ',b.cupos) as cupos_descrip,
                b.cupos as cupos,
                a.cupos as cupos_ins,
                c.fecha as fecha_hora
                FROM inscripciones a, horarios_fecha b,fechas_curso c,cursos d
                WHERE 
                c.curso_id=d.id AND
                b.fecha_curso_id=c.id AND
                a.horario_fecha_id = b.id AND 
                a.fecha_curso_id=".$curso."
                ORDER BY b.id,a.nombre_apellido";

                }
                

            
                $results=null;
                $connection = ConnectionManager::get('default');
                $inscripciones = $connection->execute($query);

                $valores = array();
                foreach ($inscripciones as $value) {
                   $valores[]=array(
                    "id"=>$value['id'],
                    "fecha_hora"=>$value['fecha_hora'],
                    "nombre"=>$value['nombre'],
                    "email"=>$value['email'],
                    "celular"=>$value['celular'],
                    "cupos"=>$value['cupos_ins'],
                    "fecha_curso"=>$value['fecha_hora'],
                    "hora"=>$value['hora']);
                }
        
                if(count($inscripciones)<=0){
                    $this->Flash->error(__('The inscripcione could not be saved. Please, try again.'));
                };
 
               
 
             }catch (\PDOException $e) {
             $error = 'No se puede borrar los datos. Esta asociado con otra clase.';
 
             }

             $this->set(compact('valores'));
             $this->set('_serialize', ['valores']);
 
    }


    public function fichero()
    {
        header("Content-type: application/vnd.ms-excel; name='excel'");
        header("Content-Disposition: filename=inscripciones.xls");
        header("Pragma: no-cache");
        header("Expires: 0");

        $datos = $this->request->data['datos_a_enviar'];
        
        if (isset($datos) && $datos != '') echo $datos;
        die();
        //$this->RequestHandler->respondAs('json');
        //$this->autoRender = false;
        //echo json_encode ($datos);
        $this->set('datos', $datos);
        $this->set('_serialize', ['datos']);
    }



    public function sendConfirmacion($values) {
        $to = $values["to"];
        $subject = "[Hola ".$values['nombre_persona']."]";
        //$fecha_letras = $this->obtenerFechaEnLetra($values["fecha"]);
        $message = '<html>
        <head>
          <meta charset="utf-8">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <title>Academia Bancard</title>
      
          <link href="//cdn-images.mailchimp.com/embedcode/slim-10_7.css" rel="stylesheet" type="text/css">
      
          <link href="https://fonts.googleapis.com/css?family=Norican" rel="stylesheet">
          <link href="https://fonts.googleapis.com/css?family=Tangerine" rel="stylesheet">
          <style>
              #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
              /* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
                 We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
              body{
                  background: #f1f1f1;
              }
              table{
                  font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
                  background: #ffffff;
              }
              td{
                  border:none;
              }
              p{
                  font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
                  font-size: 20px;
                  color: #7f7f7f;
                  padding-left: 15px;
                  padding-right: 15px;
              }
              .cyan{
                  background: #009ee2;
                  height: 28px;
              }
              .footer{
                  background: #153d89;
                  height: 130px;
              }
          </style>
        </head>
      
          <body>
              <table cellspacing="0" cellpadding="0" align="center" width="600" border="0">
                  <tr>
                      <td width="600" valign="top">
      
                          <table width="600" cellspacing="0" cellpadding="0" align="center" class="encabezado">
                              <tr>
                                  <td colspan="3" align="center">
                                      <img src="http://174.138.53.240/images/logo-mail.png" border="0" style="width:30%; margin-top:20px; margin-bottom:10px;">
                                  </td>
                              </tr>
                          </table>
                      </td>
                  </tr>
                  
                  <tr>
                      <td width="600" valign="top">
                          <table width="600" cellspacing="0" cellpadding="0" align="center" style="margin-top:5px;">
                              <tr class="cyan">
                                  <td>
                                  </td>
                              </tr>
                          </table>
                      </td>
                  </tr>
      
                  <tr>
                      <td width="600" valign="top">
                          <table width="500" cellspacing="0" cellpadding="0" align="center" style="margin-top: 10px; margin-bottom: 15px;">
                              <tr>
                                  <td>
                                      <p>Te inscribiste correctamente a la Academia Bancard. Elegiste la fecha: <b>'.$values["fecha"].'</b>. El curso comienza a las <b>'.$values['hora_ini'].' hs.</b> y culmina a las <b>'.$values['hora_fin'].' hs.</b></p>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      <p style="color:#153d89; font-size:22px; font-weight:500;">Contamos con tu presencia.</p>
                                  </td>
                              </tr>
                          </table>
                      </td>
                  </tr>
                  
                  <tr>
                      <td width="600" valign="top">
                          <table width="600" cellspacing="0" cellpadding="0" align="center" class="footer" style="margin-top:5px;">
                              <tr>
                                  <td align="center">
                                      <a href="https://www.facebook.com/redinfonetpy/" target="_blank"><img src="http://174.138.53.240/images/ico-fb.png" style="margin-left:4px; margin-right:4px;"></a><a href="https://www.instagram.com/redinfonet/" target="_blank"><img src="http://174.138.53.240/images/ico-ig.png" style="margin-left:4px; margin-right:4px;"></a><a href="https://www.youtube.com/channel/UCNuca9dzJTcfxk6Me2fKprw" target="_blank"><img src="http://174.138.53.240/images/ico-yt.png" style="margin-left:4px; margin-right:4px;"></a><a href="http://www.bancard.com.py/" target="_blank"><img src="http://174.138.53.240/images/ico-web.png" style="margin-left:4px; margin-right:4px;"></a><br>
                                      <a href="http://www.bancard.com.py/" target="_blank"><img src="http://174.138.53.240/images/infonet.png" style="margin-top:5px;"></a>
                                  </td>
                              </tr>
                          </table>
                      </td>
                  </tr>
                  
              </table>
          </body>
      </html>';

        $email = new Email();
        $email ->transport('mailjet')
               ->emailFormat('html')
               ->to($to)
               ->from('info@bancard.com.py')
               ->subject($subject);
        $email->send($message);
        return;

    
    }   

    public function sendRecordatorio($values) {
        $to = $values["to"];
        $subject = $values["subject"];
   
        $message = '
        <html>
        <head>
          <meta charset="utf-8">
          <meta http-equiv="X-UA-Compatible" content="IE=edge">
          <meta name="viewport" content="width=device-width, initial-scale=1">
          <title>Academia Bancard</title>
      
          <link href="//cdn-images.mailchimp.com/embedcode/slim-10_7.css" rel="stylesheet" type="text/css">
      
          <link href="https://fonts.googleapis.com/css?family=Norican" rel="stylesheet">
          <link href="https://fonts.googleapis.com/css?family=Tangerine" rel="stylesheet">
          <style>
              #mc_embed_signup{background:#fff; clear:left; font:14px Helvetica,Arial,sans-serif; }
              /* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
                 We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */
              body{
                  background: #f1f1f1;
              }
              table{
                  font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
                  background: #ffffff;
              }
              td{
                  border:none;
              }
              p{
                  font-family: Arial, "Helvetica Neue", Helvetica, sans-serif;
                  font-size: 20px;
                  color: #7f7f7f;
                  padding-left: 15px;
                  padding-right: 15px;
              }
              .cyan{
                  background: #009ee2;
                  height: 28px;
              }
              .footer{
                  background: #153d89;
                  height: 130px;
              }
          </style>
        </head>
      
          <body>
              <table cellspacing="0" cellpadding="0" align="center" width="600" border="0">
                  <tr>
                      <td width="600" valign="top">
      
                          <table width="600" cellspacing="0" cellpadding="0" align="center" class="encabezado">
                              <tr>
                                  <td colspan="3" align="center">
                                      <img src="http://174.138.53.240/images/logo-mail.png" border="0" style="width:30%; margin-top:20px; margin-bottom:10px;">
                                  </td>
                              </tr>
                          </table>
                      </td>
                  </tr>
                  
                  <tr>
                      <td width="600" valign="top">
                          <table width="600" cellspacing="0" cellpadding="0" align="center" style="margin-top:5px;">
                              <tr class="cyan">
                                  <td>
                                  </td>
                              </tr>
                          </table>
                      </td>
                  </tr>
      
                  <tr>
                      <td width="600" valign="top">
                          <table width="500" cellspacing="0" cellpadding="0" align="center" style="margin-top: 10px; margin-bottom: 15px;">
                              <tr>
                                  <td>
                                      <p>'.$values["mensaje"].'</p>
                                  </td>
                              </tr>
                              <tr>
                                  <td>
                                      <p style="color:#153d89; font-size:22px; font-weight:500;">Contamos con tu presencia.</p>
                                  </td>
                              </tr>
                          </table>
                      </td>
                  </tr>
                  
                  <tr>
                      <td width="600" valign="top">
                          <table width="600" cellspacing="0" cellpadding="0" align="center" class="footer" style="margin-top:5px;">
                              <tr>
                                  <td align="center">
                                      <a href="https://www.facebook.com/redinfonetpy/" target="_blank"><img src="http://174.138.53.240/images/ico-fb.png" style="margin-left:4px; margin-right:4px;"></a><a href="https://www.instagram.com/redinfonet/" target="_blank"><img src="http://174.138.53.240/images/ico-ig.png" style="margin-left:4px; margin-right:4px;"></a><a href="https://www.youtube.com/channel/UCNuca9dzJTcfxk6Me2fKprw" target="_blank"><img src="http://174.138.53.240/images/ico-yt.png" style="margin-left:4px; margin-right:4px;"></a><a href="http://www.bancard.com.py/" target="_blank"><img src="http://174.138.53.240/images/ico-web.png" style="margin-left:4px; margin-right:4px;"></a><br>
                                      <a href="http://www.bancard.com.py/" target="_blank"><img src="http://174.138.53.240/images/infonet.png" style="margin-top:5px;"></a>
                                  </td>
                              </tr>
                          </table>
                      </td>
                  </tr>
                  
              </table>
          </body>
      </html>';

        $email = new Email();
        $email ->transport('mailjet')
               ->emailFormat('html')
               ->to($to)
               ->from('info@bancard.com.py')
               ->subject($subject);
        $email->send($message);
        return;

    
    }   

}
