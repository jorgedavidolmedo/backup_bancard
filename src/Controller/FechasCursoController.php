<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Log\Log;
use Cake\Datasource\ConnectionManager;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;

/**
 * FechasCurso Controller
 *
 * @property \App\Model\Table\FechasCursoTable $FechasCurso
 */
class FechasCursoController extends AppController
{

    public function beforeFilter(Event $event)
    {

        parent::beforeFilter($event);
        $this->Auth->allow(['getFechasCurso']);
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Cursos'],
            'order'=>['FechasCurso.id DESC'],
        ];
        $fechasCurso = $this->paginate($this->FechasCurso);

        $this->set(compact('fechasCurso'));
        $this->set('_serialize', ['fechasCurso']);
    }
    /**
     * View method
     *
     * @param string|null $id Fechas Curso id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $fechasCurso = $this->FechasCurso->get($id, [
            'contain' => ['Cursos']
        ]);

        $this->set('fechasCurso', $fechasCurso);
        $this->set('_serialize', ['fechasCurso']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $fechasCurso = $this->FechasCurso->newEntity();
        if ($this->request->is('post')) {
            $fechasCurso = $this->FechasCurso->patchEntity($fechasCurso, $this->request->data);
            if ($this->FechasCurso->save($fechasCurso)) {
                $this->Flash->success(__('The fechas curso has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The fechas curso could not be saved. Please, try again.'));
            }
        }
        $cursos = $this->FechasCurso->Cursos->find('list', ['limit' => 200]);
        $this->set(compact('fechasCurso', 'cursos'));
        $this->set('_serialize', ['fechasCurso']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Fechas Curso id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $fechasCurso = $this->FechasCurso->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $fechasCurso = $this->FechasCurso->patchEntity($fechasCurso, $this->request->data);
            if ($this->FechasCurso->save($fechasCurso)) {
                $this->Flash->success(__('The fechas curso has been saved.'));

                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The fechas curso could not be saved. Please, try again.'));
            }
        }

           

        $cursos = $this->FechasCurso->Cursos->find('list', ['limit' => 200]);
        $this->set(compact('fechasCurso', 'cursos'));
        $this->set('_serialize', ['fechasCurso']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Fechas Curso id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $fechasCurso = $this->FechasCurso->get($id);
        if ($this->FechasCurso->delete($fechasCurso)) {
            $this->Flash->success(__('The fechas curso has been deleted.'));
        } else {
            $this->Flash->error(__('The fechas curso could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /********************************* SERVICES**********************************************/

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function addEntity()
    {
        if ($this->request->is('post')) {

            $conn = ConnectionManager::get('default');
            $conn->begin();

            try{

                $fcurso = $this->FechasCurso->newEntity($this->request->data);

                if ($this->FechasCurso->save($fcurso)) {
                    $conn->commit();
                    $mensaje = "ok";
                    $this->Flash->success(__('El registro se guardo correctamente.'));
                } else {
                    $conn->rollback();
                    $mensaje = "error";
                }

            }catch (\PDOException $e)
            {
                $conn->rollback();
                $mensaje = $e;
                $this->Flash->error(__('Error al guardar. vuelva a intentar.'.$e));
            }
        }

        $this->set([
            'mensaje' => $mensaje,
            'fcurso' => $fcurso,
            '_serialize' => ['mensaje', 'fcurso']
        ]);
        $this->viewClass = 'Json';
        $this->render();

    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function deleteEntity($id = null)
    {
        $message = "";
        $conn = ConnectionManager::get('default');
        try{
            $query = $conn->execute("DELETE FROM fechas_curso WHERE id=".$id);
            if($query){
                $message = "ok";
            }else{
                $message = "no se pudo borrar el registro.";
            }

        }catch (\PDOException $e)
        {
            $mensaje = "error al eliminar.";
            $this->Flash->error(__('Error al eliminar. vuelva a intentar.'));
        }

        $this->set([
            'message' => $message,
            '_serialize' => ['message']
        ]);
        $this->viewClass = 'Json';
        $this->render();

    }

    /**
     * Add method
     * 
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function getEntity($id = null)
    {

        $fcurso = $this->FechasCurso->find('all',
            array('conditions'=>array('FechasCurso.id'=>$id),
                  'contain'=>array('Cursos')));

        $this->set([
            'fcurso' => $fcurso,
            '_serialize' => ['fcurso']
        ]);
        $this->viewClass = 'Json';
        $this->render();

    }

     /**
     * Edit method
     *
     * @param string|null $id Usuario id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function editEntity($id = null)
    {

        $fcurso =$this->FechasCurso->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            try{
                $fcurso = $this->FechasCurso->patchEntity($fcurso, $this->request->data);
                if ($this->FechasCurso->save($fcurso)) {
                    $mensaje = "ok.";
                    $this->Flash->success(__('El registro se modifico correctamente.'));
                } else {
                    $mensaje = "error al modificar.";
                }

            }catch (\PDOException $e)
            {

                $mensaje = "error al editar.";
                $this->Flash->error(__('Error al editar. vuelva a intentar.'));
            }


        }

        $this->set([
            'mensaje' => $mensaje,
            'fcurso' => $fcurso,
            '_serialize' => ['mensaje', 'fcurso']
        ]);
        $this->viewClass = 'Json';
        $this->render();
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function getFechaCursos()
    {
        if ($this->request->is('get')) {
            $conn = ConnectionManager::get('default');
            $conn->begin();
            $fcursos = [];
            try{
                 $sql = "SELECT a.id as id, a.fecha as fecha, b.nombre as nombre,
                 a.curso_id as id_curso
                 FROM fechas_curso a, cursos b
                 WHERE
                 a.curso_id=b.id";
               
                 $results = $conn->execute($sql);

                 $resultado = array();
                 foreach ($results as $value){
                     $resultado[] = array("id"=>$value['id'],
                        "curso"=> $value['nombre'].' - ['. date('d/m/Y',strtotime($value['fecha'])).']',
                        "fecha"=>$value['fecha'],
                        "curso_id"=>$value['id_curso']);
                 }
                $fcursos = $resultado;
                $mensaje = "202";
            }catch (\PDOException $e)
            {
                $conn->rollback();
                $mensaje = $e;
                $this->Flash->error(__('Error al guardar. vuelva a intentar.'.$e));
            }
        }
        $this->set([
            'mensaje' => $mensaje,
            'fcursos' => $fcursos,
            '_serialize' => ['mensaje', 'fcursos']
        ]);
        $this->viewClass = 'Json';
        $this->render();

    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function verificar($fecha=null)
    {
        $fecha_actual = "";
        if ($this->request->is('get')) {
            $conn = ConnectionManager::get('default');

            $hoy = date("Y-m-d");

            if($fecha>=$hoy){
                $mensaje="guardar";
                $estado = 1;
            }

            if($fecha<$hoy){
                $estado = 2;
                $mensaje="La Fecha no puede ser menor a la de hoy!";
            }

            $sql = "SELECT id,fecha FROM fechas_curso WHERE fecha='".$fecha."'";
            $results = $conn->execute($sql);
            $fecha_estado = 0;
            foreach ($results as $value){
                 $fecha_estado=1; 
                 $fecha_actual = date('d/m/Y',strtotime($value['fecha']));   
            }
            if($fecha_estado==1){
                $mensaje="Esta fecha ya tiene una capacitacion, por favor cambie de fecha!";
                $estado = 3;
            }
            
        }
        $this->set([
            'mensaje' => $mensaje,
            'estado' => $estado,
            'fecha_actual' => $fecha_actual,
            '_serialize' => ['mensaje', 'estado','fecha_actual']
        ]);
        $this->viewClass = 'Json';
        $this->render();

    }

      /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function verificarEdit($fecha=null)
    {
        if ($this->request->is('get')) {
            $conn = ConnectionManager::get('default');

            $hoy = date("Y-m-d");

            if($fecha<$hoy){
                $estado = 2;
                $mensaje="No puedes modificar una fecha anterior!";
            }else{
                $estado=1;
                $mensaje="modificar";
            }
            
        }
        $this->set([
            'mensaje' => $mensaje,
            'estado' => $estado,
            '_serialize' => ['mensaje', 'estado']
        ]);
        $this->viewClass = 'Json';
        $this->render();

    }

     /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function getFechasCurso()
    {
        $this->response->header(array(
            'Access-Control-Allow-Origin' => '*',
             'Access-Control-Allow-Headers' => 'Content-Type'
        ));
        if ($this->request->is('get')) {
            $conn = ConnectionManager::get('default');
            $conn->begin();
            $fechas_curso = [];
            try{
                 $sql = "SELECT a.id as id, a.fecha as fecha, b.nombre as nombre,
                 a.curso_id as id_curso
                 FROM fechas_curso a, cursos b
                 WHERE
                 a.curso_id=b.id";
               
                 $results = $conn->execute($sql);

                 $resultado = array();
                 foreach ($results as $value){
                     $resultado[] = array("id"=>$value['id'],
                        "curso"=> $value['nombre'].' - ['. date('d/m/Y',strtotime($value['fecha'])).']',
                        "fecha"=>$value['fecha'],
                        "curso_id"=>$value['id_curso']);
                 }
                $fechas_curso = $resultado;
                $mensaje = "202";
            }catch (\PDOException $e)
            {
                $conn->rollback();
                $mensaje = $e;
                $this->Flash->error(__('Error al guardar. vuelva a intentar.'.$e));
            }
        }
        $this->set([
            'mensaje' => $mensaje,
            'fechas_curso' => $fechas_curso,
            '_serialize' => ['mensaje', 'fechas_curso']
        ]);
        $this->viewClass = 'Json';
        $this->render();

    }
}
