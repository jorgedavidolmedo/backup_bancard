<?= $this->Html->script('controladores/bancard/fechaCursoController');?>
<?php $id_empresa = $this->request->session()->read('id_empresa');?>
<div class="content">
    <?php
        $aplicacion = $this->request->session()->read("aplicacion");
        $this->Html->addCrumb($aplicacion,'/');
        $this->Html->addCrumb('Fechas Por Curso', '/FechasCurso/index');
        echo $this->Html->getCrumbList();
        
    ?>
</div>
<div class="container" ng-app="bancard" ng-controller="fechaCursoEdit" ng-init="cargar_datos(<?php echo $fechasCurso->id;?>)">
    <div class="panel panel-default panel-table">
        <div class="panel-heading">
            <div class="row">
                <div class="col col-xs-6">
                    <h3 class="panel-title">Editar Fecha Por Curso</h3>
                </div>
                <div class="col col-xs-6 text-right">
                    <?= $this->Html->link($this->Html->tag('span','',
                    ['class' => 'glyphicon glyphicon-list']), 
                    ['controller'=>'FechasCurso','action' => 'index'],['escape' => false,'class'=>'btn btn-default']) ?>
                </div>
            </div>
        </div>
       
        <div class="panel-body">


             <div class="row">
                <div class="col-xs-12 col-sm-2 col-md-2">
                        
                        <?php
                            echo $this->Form->input('fecha',array('type' => 'text','class' => 'myDate',
                            'label'=>'Fecha','id'=>'fecha','required',
                            'autocomplete'=>'off'));

                        ?>
                
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6">
                        <label class="control-label" for="customer">Curso</label>

                        <select class="form-control" name="curso" id="curso" 
                        ng-options="item.nombre for item in cursos" ng-model="selectedCurso" 
                        ng-change="hasChanged()" min="1">

                        </select>
                </div>


            </div>
            <br>
          
            <div class="row">
               
                <div class="col-xs-12 col-sm-6 col-md-6">
                    <?= $this->Form->button(__('Guardar'),['class'=>'btn btn-info','ng-click'=>"verificar_fechas($fechasCurso->id)"]) ?>
                    <?= $this->Html->link(__('Cancelar'), ['action' => 'index'],['class'=>'btn btn-danger']) ?>

                </div>
            </div>

        </div>
    </div>
</div>



