<div class="content">
    <div class="content">
        <?php
            $aplicacion = $this->request->session()->read("aplicacion");
            $this->Html->addCrumb($aplicacion,'/');
            $this->Html->addCrumb('Fechas Por Curso', '/FechasCurso/index/');
            $this->Html->addCrumb('Detalle','');
            echo $this->Html->getCrumbList();
        ?>
    </div>
    <div class="panel panel-default panel-table">
        <div class="panel-heading">
            <div class="row">
                <div class="col col-xs-6">
                    <h3 class="panel-title">Fecha Por Curso</h3>
                </div>
                <div class="col col-xs-6 text-right">
                    <div class="col col-xs-6 pull-right">
                        <?=$this->Html->link('Listar', ['controller' => 'FechasCurso', 'action' => 'index'])?>
                        <?=$this->Html->link('Agregar', ['controller' => 'FechasCurso', 'action' => 'add', $fechasCurso->id])?>
                        <?=$this->Html->link('Borrar', ['controller' => 'FechasCurso', 'action' => 'delete', $fechasCurso->id])?>

                       
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-body">

            <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#usuario" role="tab" data-toggle="tab">Fecha Por Curso</a></li>
            

            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="usuario">
                    <div class="col-md-6 table-view">
                        <div class="panel">


                            <dt><?= __('Id') ?></dt>
                            <dd><?= $this->Number->format($fechasCurso->id) ?></dd></br>

                            <dt><?= __('Fecha') ?></dt>
                            <dd><?= h(date('d/m/Y',strtotime($fechasCurso->fecha))) ?></dd></br>

                            <dt><?= __('Curso') ?></dt>
                            <dd><?= h($fechasCurso->curso->nombre) ?></dd></br>

                            
                        </div>
                    </div>

                </div>
                <div role="tabpanel" class="tab-pane" id="audit">
                    <div class="col-md-6 table-view">
                        <div class="panel">

                            <dt><?= __('Creado') ?></dt>
                            <dd><?= h($usuario->created) ?></dd></br>

                            <dt><?= __('Última modificiación') ?></dt>
                            <dd><?= h($usuario->modified) ?></dd></br>

                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>

<style>

    .standar{
        cursor: pointer;
        font-size: 16px;
        padding: 5px;
    }

</style>
