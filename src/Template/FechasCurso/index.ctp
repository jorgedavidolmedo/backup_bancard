<?= $this->Html->script('controladores/bancard/fechaCursoController');?>
<div class="content" ng-app="bancard" ng-controller="fechaCursoIndex">
    <?php
    $aplicacion = $this->request->session()->read("aplicacion");
    $this->Html->addCrumb($aplicacion,'/');
    $this->Html->addCrumb('Fechas Por Curso', '');

    echo $this->Html->getCrumbList();
    ?>
    <div class="panel panel-default panel-table">
        <div class="panel-heading">
            <div class="row">
                <div class="col col-xs-6">
                    <h3 class="panel-title">Fechas Por Curso</h3>
                </div>
                <div class="col col-xs-6 text-right">
                    <?= $this->Html->link($this->Html->tag('span','',
                    ['class' => 'glyphicon glyphicon-plus']), 
                    ['controller'=>'FechasCurso','action' => 'add'],['escape' => false,'class'=>'btn btn-symbol']) ?>


                </div>
            </div>
        </div>

        <div class="panel-body">
            <div class="search-box">

                <div class="row">
                    <div class="col-xs-4 col-sm-4 col-md-4">
                         <?=$this->Form->input('buscador',array("id"=>"filtrar",'label'=>'','type'=>'text','placeholder'=>'Buscar...','class'=>'search form-control buscador','name'=>'filtrar'))?>
                    </div>
                </div>

            </div>

            <table class="table table-responsive table-striped table-bordered table-list table table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Fecha</th>
                    <th>Curso</th>
                    
                    
                    <th  width="10%" class="text-center actions"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span></th>
                </tr>

                </thead>
                <tbody class="buscar">

                     <?php foreach ($fechasCurso as $value):?>
                       <tr>

                         <td><?= $this->Number->format($value->id) ?></td>
                         <td><?= $value->has('fecha') ? $this->Html->link(h(date('d/m/Y',strtotime($value->fecha))), ['controller' => 'FechasCurso', 'action' => 'view', $value->id]) : '' ?></td>
                         <td><?= $value->curso->nombre; ?></td>
                         
                           <td class="actions">
                           <a class="glyphicon glyphicon-pencil standar" ng-click = "verificar_fecha_edit(<?php echo $value->id;?>,'<?php echo date('d/m/Y',strtotime($value->fecha));?>')"></a>
                            <a class="glyphicon glyphicon-remove standar" ng-click = "borrar_entity(<?php echo $value->id;?>)"></a>
                             </td>

                       </tr>
                     <?php endforeach;?>

                </tbody>
            </table>

            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                </ul>

            </div>

        </div>

    </div>
</div>

<style>
a{
    cursor:pointer;
}
</style>
