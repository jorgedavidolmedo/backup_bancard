<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Horarios Fecha'), ['action' => 'edit', $horariosFecha->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Horarios Fecha'), ['action' => 'delete', $horariosFecha->id], ['confirm' => __('Are you sure you want to delete # {0}?', $horariosFecha->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Horarios Fecha'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Horarios Fecha'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Fechas Curso'), ['controller' => 'FechasCurso', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Fechas Curso'), ['controller' => 'FechasCurso', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="horariosFecha view large-9 medium-8 columns content">
    <h3><?= h($horariosFecha->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Fechas Curso') ?></th>
            <td><?= $horariosFecha->has('fechas_curso') ? $this->Html->link($horariosFecha->fechas_curso->id, ['controller' => 'FechasCurso', 'action' => 'view', $horariosFecha->fechas_curso->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($horariosFecha->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Cupos') ?></th>
            <td><?= $this->Number->format($horariosFecha->cupos) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Horario Inicio') ?></th>
            <td><?= h($horariosFecha->horario_inicio) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Horario Fin') ?></th>
            <td><?= h($horariosFecha->horario_fin) ?></td>
        </tr>
    </table>
</div>
