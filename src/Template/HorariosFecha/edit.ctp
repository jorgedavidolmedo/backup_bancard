<?= $this->Html->script('controladores/bancard/horarioFechaController');?>
<?php $id_empresa = $this->request->session()->read('id_empresa');?>
<div class="content">
    <?php
        $aplicacion = $this->request->session()->read("aplicacion");
        $this->Html->addCrumb($aplicacion,'/');
        $this->Html->addCrumb('Editar Por Fechas', '/HorariosFechas/index');
        echo $this->Html->getCrumbList();
    ?>
</div>
<div class="container" ng-app="bancard" ng-controller="horarioFechaEdit" ng-init="cargar_datos(<?php echo $horariosFecha->id;?>)">
    <div class="panel panel-default panel-table">
        <div class="panel-heading">
            <div class="row">
                <div class="col col-xs-6">
                    <h3 class="panel-title">Editar Horario Por Fecha</h3>
                </div>
                <div class="col col-xs-6 text-right">
                    <?= $this->Html->link($this->Html->tag('span','',
                    ['class' => 'glyphicon glyphicon-list']), 
                    ['controller'=>'HorariosFecha','action' => 'index'],['escape' => false,'class'=>'btn btn-default']) ?>
                </div>
            </div>
        </div>
       
        <div class="panel-body">


             <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6">
                        <label class="control-label" for="customer">Fecha Curso</label>

                        <select class="form-control" name="curso" id="curso" 
                        ng-options="item.curso for item in cursos track by item.id" ng-model="fechas_curso" 
                        ng-change="hasChanged()" min="1">

                        </select>
                </div>


            </div>
            <br>
          
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-4">
                        
                        <?php
                            echo $this->Form->input('inicio',array('type' => 'text','class' => 'myTime',
                            'label'=>'Hora Inicio','id'=>'inicio','required',
                            'autocomplete'=>'off'));

                        ?>
                
                </div>
            </div>

            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-4">
                        
                        <?php
                            echo $this->Form->input('fin',array('type' => 'text','class' => 'myTime',
                            'label'=>'Hora Fin','id'=>'fin','required',
                            'autocomplete'=>'off'));

                        ?>
                
                </div>
            </div>

             <div class="row">
                <div class="col-xs-12 col-sm-2 col-md-2">
                    <?=$this->Form->input('cupo',array('class' => 'form-control',
                    'label'=>'Cupos','ng-model'=>'hfecha.cupos'))?>
                </div>

            </div>

            
            <br>
          
            <div class="row">
               
                <div class="col-xs-12 col-sm-6 col-md-6">
                <?= $this->Form->button(__('Guardar'),['class'=>'btn btn-info','ng-click'=>"modificar($horariosFecha->id)"]) ?>
                    <?= $this->Html->link(__('Cancelar'), ['action' => 'index'],['class'=>'btn btn-danger']) ?>

                </div>
            </div>

        </div>
    </div>
</div>



