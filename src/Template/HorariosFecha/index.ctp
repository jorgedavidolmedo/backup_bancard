<?= $this->Html->script('controladores/bancard/horarioFechaController');?>
<div class="content" ng-app="bancard" ng-controller="horarioFechaIndex">
    <?php
    $aplicacion = $this->request->session()->read("aplicacion");
    $this->Html->addCrumb($aplicacion,'/');
    $this->Html->addCrumb('Horarios Por Fecha', '');

    echo $this->Html->getCrumbList();
    ?>
    <div class="panel panel-default panel-table">
        <div class="panel-heading">
            <div class="row">
                <div class="col col-xs-6">
                    <h3 class="panel-title">Horarios Por Fecha</h3>
                </div>
                <div class="col col-xs-6 text-right">
                    <?= $this->Html->link($this->Html->tag('span','',
                    ['class' => 'glyphicon glyphicon-plus']), 
                    ['controller'=>'HorariosFecha','action' => 'add'],['escape' => false,'class'=>'btn btn-symbol']) ?>


                </div>
            </div>
        </div>

        <div class="panel-body">
            <div class="search-box">

                <div class="row">
                    <div class="col-xs-4 col-sm-4 col-md-4">
                         <?=$this->Form->input('buscador',array("id"=>"filtrar",'label'=>'','type'=>'text','placeholder'=>'Buscar...','class'=>'search form-control buscador','name'=>'filtrar'))?>
                    </div>
                </div>

            </div>

            <table class="table table-responsive table-striped table-bordered table-list table table-hover">
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Curso</th>
                    <th>Hora Inicio</th>
                    <th>Hora Fin</th>
                    <th>Cupos</th>
                   
                    <th  width="10%" class="text-center actions"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span></th>
                </tr>

                </thead>
                <tbody class="buscar">

                     <?php foreach ($horariosFecha as $value):?>
                       <tr>
                         <td><?= $this->Number->format($value->id) ?></td>
                         <td><?= $value->fechas_curso->has('fecha') ? $this->Html->link($value->fechas_curso->curso->nombre." - [".(date('d/m/Y',strtotime($value->fechas_curso->fecha)))."]", ['controller' => 'FechasCurso', 'action' => 'view', $value->fechas_curso->id]) : '' ?></td>
                         <td><?= $value->horario_inicio->format("H:i");?></td>
                         <td><?= $value->horario_fin->format("H:i");?></td>
                         <td><?= ($value->cupos) ?></td>
                        <td class="actions">
                           <a class="glyphicon glyphicon-pencil standar" ng-click = "obtener_entity(<?php echo $value->id;?>)"></a>
                            <a class="glyphicon glyphicon-remove standar" ng-click = "borrar_entity(<?php echo $value->id;?>)"></a>
                             </td>

                       </tr>
                     <?php endforeach;?>

                </tbody>
            </table>

            <div class="paginator">
                <ul class="pagination">
                    <?= $this->Paginator->prev('< ' . __('previous')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('next') . ' >') ?>
                </ul>

            </div>

        </div>

    </div>
</div>

<style>
a{
    cursor:pointer;
}
</style>