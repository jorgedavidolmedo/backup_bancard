<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Curso'), ['action' => 'edit', $curso->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Curso'), ['action' => 'delete', $curso->id], ['confirm' => __('Are you sure you want to delete # {0}?', $curso->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Cursos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Curso'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Fechas Curso'), ['controller' => 'FechasCurso', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Fechas Curso'), ['controller' => 'FechasCurso', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Inscripciones'), ['controller' => 'Inscripciones', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Inscripcione'), ['controller' => 'Inscripciones', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="cursos view large-9 medium-8 columns content">
    <h3><?= h($curso->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Nombre') ?></th>
            <td><?= h($curso->nombre) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($curso->id) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Fechas Curso') ?></h4>
        <?php if (!empty($curso->fechas_curso)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Fecha') ?></th>
                <th scope="col"><?= __('Curso Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($curso->fechas_curso as $fechasCurso): ?>
            <tr>
                <td><?= h($fechasCurso->id) ?></td>
                <td><?= h($fechasCurso->fecha) ?></td>
                <td><?= h($fechasCurso->curso_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'FechasCurso', 'action' => 'view', $fechasCurso->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'FechasCurso', 'action' => 'edit', $fechasCurso->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'FechasCurso', 'action' => 'delete', $fechasCurso->id], ['confirm' => __('Are you sure you want to delete # {0}?', $fechasCurso->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Inscripciones') ?></h4>
        <?php if (!empty($curso->inscripciones)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Nombre Apellido') ?></th>
                <th scope="col"><?= __('Cupos') ?></th>
                <th scope="col"><?= __('Curso Id') ?></th>
                <th scope="col"><?= __('Fecha Curso Id') ?></th>
                <th scope="col"><?= __('Horario Fecha Id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($curso->inscripciones as $inscripciones): ?>
            <tr>
                <td><?= h($inscripciones->id) ?></td>
                <td><?= h($inscripciones->nombre_apellido) ?></td>
                <td><?= h($inscripciones->cupos) ?></td>
                <td><?= h($inscripciones->curso_id) ?></td>
                <td><?= h($inscripciones->fecha_curso_id) ?></td>
                <td><?= h($inscripciones->horario_fecha_id) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Inscripciones', 'action' => 'view', $inscripciones->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Inscripciones', 'action' => 'edit', $inscripciones->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Inscripciones', 'action' => 'delete', $inscripciones->id], ['confirm' => __('Are you sure you want to delete # {0}?', $inscripciones->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
