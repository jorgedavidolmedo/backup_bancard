<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $inscripcione->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $inscripcione->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Inscripciones'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Cursos'), ['controller' => 'Cursos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Curso'), ['controller' => 'Cursos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Fechas Curso'), ['controller' => 'FechasCurso', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Fechas Curso'), ['controller' => 'FechasCurso', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Horarios Fecha'), ['controller' => 'HorariosFecha', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Horarios Fecha'), ['controller' => 'HorariosFecha', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="inscripciones form large-9 medium-8 columns content">
    <?= $this->Form->create($inscripcione) ?>
    <fieldset>
        <legend><?= __('Edit Inscripcione') ?></legend>
        <?php
            echo $this->Form->input('nombre_apellido');
            echo $this->Form->input('cupos');
            echo $this->Form->input('curso_id', ['options' => $cursos]);
            echo $this->Form->input('fecha_curso_id', ['options' => $fechasCurso]);
            echo $this->Form->input('horario_fecha_id', ['options' => $horariosFecha]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
