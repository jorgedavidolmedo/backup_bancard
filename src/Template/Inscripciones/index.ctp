<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Inscripcione'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Cursos'), ['controller' => 'Cursos', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Curso'), ['controller' => 'Cursos', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Fechas Curso'), ['controller' => 'FechasCurso', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Fechas Curso'), ['controller' => 'FechasCurso', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Horarios Fecha'), ['controller' => 'HorariosFecha', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Horarios Fecha'), ['controller' => 'HorariosFecha', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="inscripciones index large-9 medium-8 columns content">
    <h3><?= __('Inscripciones') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('nombre_apellido') ?></th>
                <th scope="col"><?= $this->Paginator->sort('cupos') ?></th>
                <th scope="col"><?= $this->Paginator->sort('curso_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('fecha_curso_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('horario_fecha_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($inscripciones as $inscripcione): ?>
            <tr>
                <td><?= $this->Number->format($inscripcione->id) ?></td>
                <td><?= h($inscripcione->nombre_apellido) ?></td>
                <td><?= $this->Number->format($inscripcione->cupos) ?></td>
                <td><?= $inscripcione->has('curso') ? $this->Html->link($inscripcione->curso->id, ['controller' => 'Cursos', 'action' => 'view', $inscripcione->curso->id]) : '' ?></td>
                <td><?= $inscripcione->has('fechas_curso') ? $this->Html->link($inscripcione->fechas_curso->id, ['controller' => 'FechasCurso', 'action' => 'view', $inscripcione->fechas_curso->id]) : '' ?></td>
                <td><?= $inscripcione->has('horarios_fecha') ? $this->Html->link($inscripcione->horarios_fecha->id, ['controller' => 'HorariosFecha', 'action' => 'view', $inscripcione->horarios_fecha->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $inscripcione->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $inscripcione->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $inscripcione->id], ['confirm' => __('Are you sure you want to delete # {0}?', $inscripcione->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
