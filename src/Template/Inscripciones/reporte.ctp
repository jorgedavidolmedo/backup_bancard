<?= $this->Html->script('controladores/bancard/usuariosController');?>
<div ng-app="bancard" ng-controller="usuarioIndex">
<div class="content">
    <?php
        $id_empresa = $this->request->session()->read('id_empresa');
        $aplicacion = $this->request->session()->read("aplicacion");
        $this->Html->addCrumb($aplicacion,'/');
        $this->Html->addCrumb('Inscripciones', '');

        echo $this->Html->getCrumbList();
    ?>
</div>

<div class="container">
    <div class="panel panel-default panel-table">
        <div class="panel-heading">
            <div class="row">
                <div class="col col-xs-6">
                    <h3 class="panel-title">Inscripciones</h3>
                </div>

            </div>
        </div>

        <div class="panel-body">

         
          <div class="panel-body">
              <div class="search-box">
 <?= $this->Form->create(null,array('url' => array('action' => 'printReporte'),'target'=>'_blank')) ?>
                  <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6">
                    <label for="curso">Fecha por Curso:</label>
                    <select class="form-control" data-live-search="true" id="curso" name="curso">
                    <option value="0">Todos</option>
                  <?php foreach ($cursos as $value):?>
                       <option value="<?php echo $value['id'];?>"><?php echo $value['curso'];?></option>
                 <?php endforeach;?>  
                  </select>
                    </div>
                  </div>
                    <br>
                   <div class="row hidden">

                        <div class="col-xs-12 col-sm-3 col-md-3">
                            <?=$this->Form->input('reporte',['type'=>'select',
                                'label'=>'Reporte',
                                'options'=>["detallado" => "Detallado","resumido" => "Resumido"],'class'=>'input'])?>
                      </div>

                        </div>

              </div>

              <br>
              <div class="row">

                 <div class="col-xs-12 col-sm-6 col-md-6">
                     <?= $this->Form->button(__('Consultar'),['class'=>'btn btn-info']) ?>
                      <?= $this->Form->end() ?>
                    
                  </div>
                  
              </div>

                 <br>
                 <div class="row">

                 <div class="col-xs-12 col-sm-6 col-md-6">
                     <button class='btn btn-default' ng-click='printExcel()'>Exportar a Excel</button>
                    
                  </div>
                  
      </div>
</div>
 
  </div>
</div>
<style>

    .btn {
        font-size: 13px;
    }

    .calc{
        margin-top: 23px;
    }

    .btn-rojo:hover, .btn-default.active, .btn-default:active, .btn-default.focus, .btn-default:focus {
    background-color: #337ab761;
    color: #fff;
    outline: none !important;
}


</style>
