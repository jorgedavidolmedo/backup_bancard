<?= $this->Html->script('controladores/bancard/usuariosController');?>
<div class="content" ng-app="bancard" ng-controller="usuarioIndex">
    <?php
        $id_empresa = $this->request->session()->read('id_empresa');
        $aplicacion = $this->request->session()->read("aplicacion");
        $this->Html->addCrumb($aplicacion,'/');
        $this->Html->addCrumb('Inscripciones', '');

        echo $this->Html->getCrumbList();
    ?>
</div>

<div class="container">
    <div class="panel panel-default panel-table">
        <div class="panel-heading">
            <div class="row">
                <div class="col col-xs-6">
                    <h3 class="panel-title">Inscripciones</h3>
                </div>

            </div>
        </div>

        <div class="panel-body">

         <?= $this->Form->create(null,array('url' => array('action' => 'fichero'),'target'=>'_blank',
        "id"=>"FormularioExportacion")) ?>
          <div class="panel-body">

            <div class="col-xs-12 col-sm-6 col-md-6 excel">

            <p><img src="http://174.138.53.240/excel.png" onClick="descargar()" /><br><strong>Descargar</strong></p>
            <input type="hidden" id="datos_a_enviar" name="datos_a_enviar" />
            </div>
             
              <div id="div1">
            <table class="table table-responsive table-striped table-bordered table-list table table-hover" id="Exportar_a_Excel">
                <thead>
               <tr>
                    <th>ID</th>
                    <th>Fecha Inscripcion</th>
                    <th>Cual es tu nombre?</th>
                    <th>Email</th>
                    <th>Celular</th>
                    <th>Cupos</th>
                    <th>Fecha del Curso</th>
                    <th>Horario Seleccionado</th>
                    
                  </tr>


                </thead>
                <tbody>
                <?php $totalFactura = 0;?>
                <?php foreach ($valores as $value):?>
                    <tr>
                        <td><?= $value['id']?></td>
                        <td><?= $value['fecha_hora']?></td>
                        <td><?= $value['nombre']?></td>
                        <td><?= $value['email']?></td>
                        <td><?= $value['celular']?></td>
                        <td><?= $value['cupos']?></td>
                        <td><?= $value['fecha_curso']?></td>
                        <td><?= $value['hora']?></td>

                    </tr>
                <?php endforeach;?>

                </tbody>
               
            </table>
            </div>

          <?= $this->Form->end() ?>
        </div>

  </div>

<style>

    .btn {
        font-size: 13px;
    }

    .calc{
        margin-top: 23px;
    }

    .btn-rojo:hover, .btn-default.active, .btn-default:active, .btn-default.focus, .btn-default:focus {
    background-color: #337ab761;
    color: #fff;
    outline: none !important;
}

 .excel img {
        max-width: 10%;
        height: auto;
        cursor:hand;
    }




</style>

<script language="javascript">
$(document).ready(function() {
  $(".botonExcel").click(function(event) {
    $("#datos_a_enviar").val( $("<div>").append( $("#Exportar_a_Excel").eq(0).clone()).html());
    $("#FormularioExportacion").submit();
});

 
});

 function descargar(){
    $("#datos_a_enviar").val( $("<div>").append( $("#Exportar_a_Excel").eq(0).clone()).html());
    $("#FormularioExportacion").submit();
  }

 $(window).load(function() {
      descargar();
}); 

</script>
