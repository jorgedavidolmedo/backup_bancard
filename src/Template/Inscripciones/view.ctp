<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Inscripcione'), ['action' => 'edit', $inscripcione->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Inscripcione'), ['action' => 'delete', $inscripcione->id], ['confirm' => __('Are you sure you want to delete # {0}?', $inscripcione->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Inscripciones'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Inscripcione'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Cursos'), ['controller' => 'Cursos', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Curso'), ['controller' => 'Cursos', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Fechas Curso'), ['controller' => 'FechasCurso', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Fechas Curso'), ['controller' => 'FechasCurso', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Horarios Fecha'), ['controller' => 'HorariosFecha', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Horarios Fecha'), ['controller' => 'HorariosFecha', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="inscripciones view large-9 medium-8 columns content">
    <h3><?= h($inscripcione->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Nombre Apellido') ?></th>
            <td><?= h($inscripcione->nombre_apellido) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Curso') ?></th>
            <td><?= $inscripcione->has('curso') ? $this->Html->link($inscripcione->curso->id, ['controller' => 'Cursos', 'action' => 'view', $inscripcione->curso->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fechas Curso') ?></th>
            <td><?= $inscripcione->has('fechas_curso') ? $this->Html->link($inscripcione->fechas_curso->id, ['controller' => 'FechasCurso', 'action' => 'view', $inscripcione->fechas_curso->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Horarios Fecha') ?></th>
            <td><?= $inscripcione->has('horarios_fecha') ? $this->Html->link($inscripcione->horarios_fecha->id, ['controller' => 'HorariosFecha', 'action' => 'view', $inscripcione->horarios_fecha->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($inscripcione->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Cupos') ?></th>
            <td><?= $this->Number->format($inscripcione->cupos) ?></td>
        </tr>
    </table>
</div>
