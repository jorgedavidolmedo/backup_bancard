<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * HorariosFecha Entity
 *
 * @property int $id
 * @property int $cupos
 * @property \Cake\I18n\Time $horario_inicio
 * @property \Cake\I18n\Time $horario_fin
 * @property int $fecha_curso_id
 *
 * @property \App\Model\Entity\FechasCurso $fechas_curso
 */
class HorariosFecha extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
