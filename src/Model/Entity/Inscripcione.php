<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Inscripcione Entity
 *
 * @property int $id
 * @property string $nombre_apellido
 * @property int $cupos
 * @property int $curso_id
 * @property int $fecha_curso_id
 * @property int $horario_fecha_id
 *
 * @property \App\Model\Entity\Curso $curso
 * @property \App\Model\Entity\FechasCurso $fechas_curso
 * @property \App\Model\Entity\HorariosFecha $horarios_fecha
 */
class Inscripcione extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}
