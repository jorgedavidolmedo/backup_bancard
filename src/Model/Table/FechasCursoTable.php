<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * FechasCurso Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Cursos
 *
 * @method \App\Model\Entity\FechasCurso get($primaryKey, $options = [])
 * @method \App\Model\Entity\FechasCurso newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\FechasCurso[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\FechasCurso|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\FechasCurso patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\FechasCurso[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\FechasCurso findOrCreate($search, callable $callback = null)
 */
class FechasCursoTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('fechas_curso');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Cursos', [
            'foreignKey' => 'curso_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        /*$validator
            ->date('fecha')
            ->allowEmpty('fecha');*/

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['curso_id'], 'Cursos'));

        return $rules;
    }
}
