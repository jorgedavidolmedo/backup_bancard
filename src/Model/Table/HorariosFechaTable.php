<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * HorariosFecha Model
 *
 * @property \Cake\ORM\Association\BelongsTo $FechasCurso
 *
 * @method \App\Model\Entity\HorariosFecha get($primaryKey, $options = [])
 * @method \App\Model\Entity\HorariosFecha newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\HorariosFecha[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\HorariosFecha|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\HorariosFecha patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\HorariosFecha[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\HorariosFecha findOrCreate($search, callable $callback = null)
 */
class HorariosFechaTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('horarios_fecha');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('FechasCurso', [
            'foreignKey' => 'fecha_curso_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        /*$validator
            ->integer('cupos')
            ->allowEmpty('cupos');

        $validator
            ->time('horario_inicio')
            ->allowEmpty('horario_inicio');

        $validator
            ->time('horario_fin')
            ->allowEmpty('horario_fin');*/

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['fecha_curso_id'], 'FechasCurso'));

        return $rules;
    }
}
