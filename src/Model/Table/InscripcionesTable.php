<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Inscripciones Model
 *
 * @property \Cake\ORM\Association\BelongsTo $Cursos
 * @property \Cake\ORM\Association\BelongsTo $FechasCurso
 * @property \Cake\ORM\Association\BelongsTo $HorariosFecha
 *
 * @method \App\Model\Entity\Inscripcione get($primaryKey, $options = [])
 * @method \App\Model\Entity\Inscripcione newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Inscripcione[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Inscripcione|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Inscripcione patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Inscripcione[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Inscripcione findOrCreate($search, callable $callback = null)
 */
class InscripcionesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('inscripciones');
        $this->displayField('id');
        $this->primaryKey('id');

        $this->belongsTo('Cursos', [
            'foreignKey' => 'curso_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('FechasCurso', [
            'foreignKey' => 'fecha_curso_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('HorariosFecha', [
            'foreignKey' => 'horario_fecha_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

       /* $validator
            ->allowEmpty('nombre_apellido');

        $validator
            ->integer('cupos')
            ->allowEmpty('cupos');*/

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['curso_id'], 'Cursos'));
        $rules->add($rules->existsIn(['fecha_curso_id'], 'FechasCurso'));
        $rules->add($rules->existsIn(['horario_fecha_id'], 'HorariosFecha'));

        return $rules;
    }
}
